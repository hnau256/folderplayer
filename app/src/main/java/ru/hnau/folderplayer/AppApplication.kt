package ru.hnau.folderplayer

import android.app.Application
import ru.hnau.folderplayer.utils.managers.ContextContainer
import ru.hnau.folderplayer.widget.WidgetWideProvider


class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ContextContainer.onContextChanged(this)
        WidgetWideProvider.init()
    }

}