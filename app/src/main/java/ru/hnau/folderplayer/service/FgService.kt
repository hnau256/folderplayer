package ru.hnau.folderplayer.service

import android.app.*
import android.content.Intent
import ru.hnau.folderplayer.utils.managers.audio.PlayerManager
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManager


class FgService : Service() {

    private data class State(
            val name: String,
            val paused: Boolean
    )

    private var state: State? = null

    override fun onCreate() {
        super.onCreate()
        PlayerManager.addOnStateChangedListener(this::onPlayerStateChanged)
        onPlayerStateChanged(PlayerManager.state)
    }

    override fun onDestroy() {
        super.onDestroy()
        PlayerManager.removeOnStateChangedListener(this::onPlayerStateChanged)
    }

    private fun onStateChanged(state: State?) {
        if (state == null) {
            FgServiceNotification.hide(this)
            stopSelf()
            return
        }
        FgServiceNotification.show(this, state.name, state.paused)
    }

    private fun onPlayerStateChanged(state: PlayerManager.State) {
        val newState = playerStateToState(state)
        if (this.state == newState) {
            return
        }
        this.state = newState
        onStateChanged(newState)
    }

    private fun playerStateToState(playerState: PlayerManager.State): State? {
        val name = playerState.currentItem?.filename ?: return null
        val paused = playerState.paused
        return FgService.State(name, paused)
    }

    override fun onBind(intent: Intent) = null
}
