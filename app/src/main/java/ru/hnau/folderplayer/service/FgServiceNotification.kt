package ru.hnau.folderplayer.service

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.support.v4.app.NotificationCompat
import android.widget.RemoteViews
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManagerUserAction


object FgServiceNotification {

    private val NOTIFICATION_ID = 1

    private var remoteViews: RemoteViews? = null


    fun show(service: Service, title: String, paused: Boolean) {
        FgServiceNotificationChannel.createNotificationChannel(service)
        showNotification(service, title, paused)
    }

    fun hide(service: Service) {
        service.stopForeground(true)
    }

    private fun getRemoteViews(service: Service): RemoteViews {
        var remoteViews = this.remoteViews
        if (remoteViews == null) {
            remoteViews = generateRemoteViews(service)
            this.remoteViews = remoteViews
        }
        return remoteViews
    }

    private fun generateRemoteViews(service: Service): RemoteViews {
        val result = RemoteViews(service.packageName, R.layout.notification)
        initRemoteViews(service, result)
        return result
    }

    private fun initRemoteViews(service: Service, remoteViews: RemoteViews) {
        PlayerSessionManagerUserAction.PREV.addToView(service, remoteViews, R.id.prev)
        PlayerSessionManagerUserAction.PAUSE_OR_RESUME.addToView(service, remoteViews, R.id.pause_or_resume)
        PlayerSessionManagerUserAction.NEXT.addToView(service, remoteViews, R.id.next)
        PlayerSessionManagerUserAction.STOP.addToView(service, remoteViews, R.id.stop)
        PlayerSessionManagerUserAction.PREV.addToView(service, remoteViews, R.id.title)
        PlayerSessionManagerUserAction.SHOW_ACTIVITY.addToView(service, remoteViews, R.id.title)
    }

    private fun showNotification(service: Service, title: String, paused: Boolean) {
        val remoteViews = getRemoteViews(service)
        remoteViews.setTextViewText(R.id.title, title)
        remoteViews.setImageViewResource(R.id.pause_or_resume, if (paused) R.drawable.ic_media_play else R.drawable.ic_media_pause)
        val notificationBuilder = NotificationCompat.Builder(service, FgServiceNotificationChannel.CHANNEL_ID)
        notificationBuilder.setContent(remoteViews)
        notificationBuilder.setContentInfo(title)
        notificationBuilder.setTicker(title)
        notificationBuilder.setSmallIcon(if (paused) R.drawable.ic_notification_pause else R.drawable.ic_notification_play)
        val notification = notificationBuilder.build()
        notification.contentIntent = PendingIntent.getActivity(service, 0, PlayerSessionManagerUserAction.getShowActivityIntent(service), 0)
        val notificationManager = service.getSystemService(Context.NOTIFICATION_SERVICE) as?  NotificationManager
        if (!paused || notificationManager == null) {
            notification.flags = notification.flags or Notification.FLAG_NO_CLEAR
            service.startForeground(NOTIFICATION_ID, notification)
        } else {
            notification.deleteIntent = PlayerSessionManagerUserAction.STOP.getPendingIntent(service)
            service.stopForeground(false)
            notificationManager.notify(NOTIFICATION_ID, notification)
        }
    }


}