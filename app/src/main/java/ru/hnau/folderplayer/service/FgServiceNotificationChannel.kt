package ru.hnau.folderplayer.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import ru.hnau.folderplayer.R


object FgServiceNotificationChannel {

    val CHANNEL_ID = "player_service_channel"

    private var created = false

    fun createNotificationChannel(context: Context) {
        if (created) {
            return
        }
        created = true

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return
        }
        val name = context.getString(R.string.player_service_channel_name)
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(CHANNEL_ID, name, importance)
        channel.description = context.getString(R.string.player_service_channel_description)
        channel.enableLights(false)
        channel.enableVibration(false)
        channel.setSound(null, null)
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

}