package ru.hnau.folderplayer.utils.managers.ux.dialog_manager.actions

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import ru.hnau.folderplayer.activity.view.list.BaseList
import ru.hnau.folderplayer.activity.view.list.BaseListAdapter
import ru.hnau.folderplayer.utils.ui.UiUtils


class DialogActionsList(
        context: Context,
        private val actions: List<DialogAction>
) : BaseList<DialogAction>(
        context,
        LayoutType.VERTICAL
) {

    private val actionsAdapter = object : BaseListAdapter<DialogAction>() {
        override fun getItemCount() = actions.size

        override fun getItem(position: Int) = actions[position]

        override fun bindItem(view: View, item: DialogAction, type: Int) {
            (view as? DialogActionView)?.action = item
        }

        override fun generateView(type: Int) = DialogActionView(context).apply {
            layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
                topMargin = UiUtils.ELEMENTS_SEPARATION
            }
        }

    }

    init {
        baseListAdapter = actionsAdapter
    }

}