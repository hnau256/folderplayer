package ru.hnau.folderplayer.utils.managers.preferences.properties

import android.content.Context
import android.content.SharedPreferences

/**
 * Свойство логического типа
 */
class PreferencesBooleanProperty(key: String, defaultValue: Boolean = false) : PreferencesProperty<Boolean>(key, defaultValue) {

    override fun readValue(container: SharedPreferences, key: String) = container.getBoolean(key, defaultValue)

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: Boolean) {
        editor.putBoolean(key, value)
    }
}