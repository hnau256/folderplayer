package ru.hnau.folderplayer.utils.managers

import ru.hnau.folderplayer.activity.AppActivity
import ru.hnau.folderplayer.activity.screen_pages.ScreenPage
import ru.hnau.folderplayer.activity.view.bottom_animator.BottomAnimatorView
import ru.hnau.folderplayer.utils.listeners_containers.ListenersContainer


object AppActivityManager {

    var activity: AppActivity? = null
        private set(value) {
            if (field != value) {
                if (field != null) {
                    onAppActivityDestroyedListenersContainer.call()
                }
                field = value
            }
        }

    private val onAppActivityDestroyedListenersContainer = ListenersContainer()

    fun addOnAppActivityDestroyedListener(listener: () -> Unit) = onAppActivityDestroyedListenersContainer.add(listener)
    fun removeOnAppActivityDestroyedListener(listener: () -> Unit) = onAppActivityDestroyedListenersContainer.remove(listener)

    fun onAppActivityCreated(activity: AppActivity) {
        this.activity = activity
    }

    fun onAppActivityDestroyed() {
        activity = null
    }

    fun goBack() = activity?.goBack()

    fun goToPreviousScreen(force: Boolean = false) = activity?.goToPreviousScreen(force)

    fun showScreenPage(screenPage: ScreenPage<*>) = activity?.show(screenPage)

    fun reloadViews() = activity?.reloadViews()

    fun showBottomView(view: BottomAnimatorView) = activity?.showBottomView(view)

    fun hideBottomView() = activity?.hideBottomView()

}