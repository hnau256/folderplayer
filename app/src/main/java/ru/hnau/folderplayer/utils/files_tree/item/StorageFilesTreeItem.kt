package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import java.io.File


class StorageFilesTreeItem(
        context: Context,
        path: String,
        private val title: StringGetter
) : FsDirFilesTreeItem(
        context,
        path,
        DrawableGetter(R.drawable.ic_files_tree_item_storage),
        File(path)
) {

    override fun getParent() = RootFilesTreeItem(context)

    override fun getTitle() = title

}