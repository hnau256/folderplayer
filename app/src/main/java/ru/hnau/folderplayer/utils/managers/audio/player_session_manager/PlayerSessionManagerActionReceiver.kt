package ru.hnau.folderplayer.utils.managers.audio.player_session_manager

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent


class PlayerSessionManagerActionReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        PlayerSessionManagerUserAction.exeForIntent(intent)
    }

}