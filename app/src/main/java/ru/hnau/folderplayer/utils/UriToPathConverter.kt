package ru.hnau.folderplayer.utils

import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import java.net.URLDecoder

object UriToPathConverter {

    private val FILENAME_URI_PREFIX = "file://"

    fun uriToPath(context: Context, uri: Uri): String? {

        val uriString = uri.toString()
        if (uriString.startsWith(FILENAME_URI_PREFIX)) {
            val encodedResult = uriString.substring(FILENAME_URI_PREFIX.length)
            FirebaseManager.sendEvent("Path from uri", mapOf("type" to "file://"))
            return URLDecoder.decode(encodedResult, Charsets.UTF_8.displayName())
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return null
        }

        val isExternalStorageDocument = isExternalStorageDocument(uri)
        val isMediaDocument = isMediaDocument(uri)

        if (!isExternalStorageDocument && !isMediaDocument) {
            if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri).toLongOrNull() ?: return null
                val contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), id)
                FirebaseManager.sendEvent("Path from uri", mapOf("type" to "downloads"))
                return getDataColumn(context, contentUri, null, null)
            }
            return null
        }

        val docId = DocumentsContract.getDocumentId(uri) ?: return null
        val split = docId.split(":").filterNot { it.isBlank() }.toTypedArray()
        if (split.size < 2) {
            return null
        }
        val type = split[0].toLowerCase()

        if (isExternalStorageDocument && type == "primary") {
            FirebaseManager.sendEvent("Path from uri", mapOf("type" to "external storage"))
            return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
        }

        if (isMediaDocument && type == "audio") {
            val contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            val columnName = MediaStore.Files.FileColumns._ID
            val selection = "$columnName=?"
            val selectionArgs = arrayOf(split[1])
            FirebaseManager.sendEvent("Path from uri", mapOf("type" to "media audio document"))
            return getDataColumn(context, contentUri, selection, selectionArgs)
        }

        return null
    }

    @SuppressLint("Recycle")
    private fun getDataColumn(context: Context, uri: Uri, selection: String?, selectionArgs: Array<String>?): String? {
        val columnName = MediaStore.MediaColumns.DATA
        val projection = arrayOf(columnName)
        val cursor = context.contentResolver.query(uri, projection, selection, selectionArgs, null) ?: return null
        cursor.use {
            it.moveToFirst()
            val columnIndex = it.getColumnIndexOrThrow(columnName)
            return it.getString(columnIndex)
        }
    }

    private fun isExternalStorageDocument(uri: Uri) = "com.android.externalstorage.documents" == uri.authority

    private fun isDownloadsDocument(uri: Uri) = "com.android.providers.downloads.documents" == uri.authority

    private fun isMediaDocument(uri: Uri) = "com.android.providers.media.documents" == uri.authority
}
