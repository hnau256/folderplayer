package ru.hnau.folderplayer.utils.files_tree

import android.content.Context
import ru.hnau.folderplayer.utils.files_tree.item.*
import java.io.File

private val PATH_BOOKMARKS = "BOOKMARKS"
private val PATH_ROOT = "ROOT"

object FileTreeItemStorageStringConverter {

    fun getFilesTreeItem(context: Context, storageString: String) = when (storageString) {
        PATH_BOOKMARKS -> BookmarksFilesTreeItem(context)
        PATH_ROOT -> RootFilesTreeItem(context)
        else -> {
            val file = File(storageString)
            if (!file.exists()) {
                null
            } else {
                if (file.isDirectory) {
                    FsDirFilesTreeItem(context, path = storageString, file = file)
                } else {
                    FsFileFilesTreeItem.create(context, storageString, file)
                }
            }
        }
    }

}

fun FilesTreeItem.toStorageString() = when (this) {
    is FsFilesTreeItem -> path
    is BookmarksFilesTreeItem -> PATH_BOOKMARKS
    is RootFilesTreeItem -> PATH_ROOT
    else -> null
}