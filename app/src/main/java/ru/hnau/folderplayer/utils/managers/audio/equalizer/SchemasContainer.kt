package ru.hnau.folderplayer.utils.managers.audio.equalizer

import android.content.Context
import ru.hnau.folderplayer.utils.listeners_containers.ListenersDataContainer
import ru.hnau.folderplayer.utils.managers.ContextContainer
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.Logger
import ru.hnau.folderplayer.utils.managers.db.entities_utils.EqualizerSchemeDbUtils
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager

class SchemasContainer(
        private val onSchemeAddedListener: (id: Long) -> Unit,
        private val onSchemeRemovedListener: (Pair<List<Long>, Long>) -> Unit,
        private val onSchemeSelectedListener: (id: Long) -> Unit,
        private val onCurrentSchemeBandChangedListener: (schemeId: Long, band: Int, value: Float) -> Unit
) {

    private val TAG = SchemasContainer::class.java.simpleName

    private val bandsCount = EqualizerManager.bandsCount

    private val context: Context
        get() = ContextContainer.context

    private data class Item(
            val scheme: EqualizerScheme,
            val onNameChangedListenersContainer: ListenersDataContainer<String>,
            val onBandChangedListenersContainer: ListenersDataContainer<Int>
    )

    private val schemas = HashMap<Long, Item>()

    var currentSchemeId: Long = -1
        set(value) {
            if (field != value && schemas.containsKey(value)) {
                saveScheme(field)
                field = value
                PreferencesManager.equalizerSchemeId = value
                onSchemeSelectedListener.invoke(value)
            }
        }

    val ids: List<Long>
        get() = schemas.keys.toList().sorted()

    init {
        checkAndFillDefaultSchemas()
        readSchemasFromDb()
        currentSchemeId = getInitialSchemeId()
    }

    fun addOnSchemeNameChangedListener(id: Long, listener: (String) -> Unit) = schemas[id]?.onNameChangedListenersContainer?.add(listener)
    fun removeOnSchemeNameChangedListener(id: Long, listener: (String) -> Unit) = schemas[id]?.onNameChangedListenersContainer?.remove(listener)

    fun addOnSchemeBandChangedListener(id: Long, listener: (Int) -> Unit) = schemas[id]?.onBandChangedListenersContainer?.add(listener)
    fun removeOnSchemeBandChangedListener(id: Long, listener: (Int) -> Unit) = schemas[id]?.onBandChangedListenersContainer?.remove(listener)

    fun getSchemeName(id: Long) = schemas[id]?.scheme?.name ?: ""
    fun setSchemeName(id: Long, name: String) {
        schemas[id]?.scheme?.name = name
        FirebaseManager.sendEvent("Equalizer scheme renamed", mapOf("name" to name))
    }

    fun getSchemeValue(id: Long, band: Int) = schemas[id]?.scheme?.get(band) ?: 0f
    fun setSchemeValue(id: Long, band: Int, value: Float) {
        schemas[id]?.scheme?.set(band, value)
        if (id == currentSchemeId) {
            onCurrentSchemeBandChangedListener.invoke(id, band, value)
        }
    }

    fun isSchemeEditable(id: Long) = schemas[id]?.scheme?.editable ?: false

    fun duplicateScheme(id: Long): Long? {
        val scheme = schemas[id]?.scheme ?: return null
        val newId = (ids.max() ?: DefaultEqualizerSchemeTypes.MAX_ID) + 1
        val newScheme = scheme.copy(newId)
        EqualizerSchemeDbUtils.insert(context, newScheme)
        addSchemeToSchemas(newScheme)
        onSchemeAddedListener.invoke(newId)
        currentSchemeId = newId
        FirebaseManager.sendEvent("Equalizer added")
        return newId
    }

    fun removeScheme(id: Long) {
        val oldIds = ArrayList(ids)
        schemas.remove(id)
        EqualizerSchemeDbUtils.remove(context, id)
        onSchemeRemovedListener.invoke(Pair(oldIds, id))
        FirebaseManager.sendEvent("Equalizer removed")

        if (id == currentSchemeId) {
            updateCurrentIdAfterRemoving(id)
        }
    }

    private fun updateCurrentIdAfterRemoving(id: Long) {
        var currentId = id - 1
        while (currentId > 0 && !ids.contains(currentId)) {
            currentId--
        }
        if (currentId > 0) {
            currentSchemeId = currentId
            return
        }
    }

    fun saveAllSchemas() = ids.forEach { saveScheme(it) }

    private fun saveScheme(schemeId: Long) {
        val scheme = schemas[schemeId]?.scheme ?: return
        if (!scheme.changed) {
            return
        }
        EqualizerSchemeDbUtils.update(context, scheme)
        scheme.onSaved()
    }

    private fun getInitialSchemeId(): Long {
        val currentSchemeFromPreferences = PreferencesManager.equalizerSchemeId
        return if (schemas.containsKey(currentSchemeFromPreferences)) currentSchemeFromPreferences else DefaultEqualizerSchemeTypes.DEFAULT.id
    }

    private fun readSchemasFromDb() {
        val dbSchemas = EqualizerSchemeDbUtils.getAll(context)
        dbSchemas.forEach { scheme -> addSchemeToSchemas(scheme) }
    }

    private fun addSchemeToSchemas(scheme: EqualizerScheme) {
        val onNameChangedListenersContainer = ListenersDataContainer<String>()
        scheme.onNameChangedListener = { name: String -> onNameChangedListenersContainer.call(name) }

        val onBandChangedListenersContainer = ListenersDataContainer<Int>()
        scheme.onValueChangedListener = { band: Int -> onBandChangedListenersContainer.call(band) }

        val id = scheme.id
        schemas[id] = Item(
                scheme,
                onNameChangedListenersContainer,
                onBandChangedListenersContainer
        )
    }

    private fun checkAndFillDefaultSchemas() {
        val storedCount = EqualizerSchemeDbUtils.getCount(context)
        if (storedCount > 0) {
            return
        }

        Logger.d(TAG, "There is no default schemas in DB, initializing DB with default schemas...")

        val defaultSchemas = DefaultEqualizerSchemeTypes.values().map { it.getScheme(context, bandsCount) }
        EqualizerSchemeDbUtils.insertAll(context, defaultSchemas)
    }

}