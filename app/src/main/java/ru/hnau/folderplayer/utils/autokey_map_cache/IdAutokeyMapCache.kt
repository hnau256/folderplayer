package ru.hnau.folderplayer.utils.autokey_map_cache

import ru.hnau.folderplayer.utils.Utils

class IdAutokeyMapCache<V>(
        tryCount: Int = 1000
) : AutokeyMapCache<Int, V>(
        Utils::generateId,
        tryCount
)