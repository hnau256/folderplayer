package ru.hnau.folderplayer.utils.managers.audio.equalizer

import android.content.Context
import ru.hnau.folderplayer.R

enum class DefaultEqualizerSchemeTypes(val id: Long, private val nameResId: Int, private val values: List<Float>) {

    NORMAL(1, R.string.equalizer_default_scheme_name_normal, arrayListOf(0f, 0f, 0f, 0f, 0f)),
    STRINGS(2, R.string.equalizer_default_scheme_name_strings, arrayListOf(-0.3f, -0.4f, -0.4f, -0.5f, -0.5f, -0.4f, -0.4f, -0.3f, -0.2f, -0.2f, -0.2f, -0.2f, -0.1f, 0.2f, 0.3f, 0.0f, -0.2f, -0.2f)),
    SORT_BASS(3, R.string.equalizer_default_scheme_name_soft_bass, arrayListOf(0.2f, 0.3f, 0.2f, 0.0f, -0.7f, -0.4f, -0.3f, -0.3f, 0.0f, 0.2f, 0.3f, 0.0f, -0.1f, -0.1f, -0.1f, -0.4f, -0.5f, -0.7f)),
    SHIMMER(4, R.string.equalizer_default_scheme_name_shimmer, arrayListOf(0.0f, 0.0f, 0.0f, -0.2f, -0.2f, -0.7f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.4f, 0.1f, 0.3f, 0.3f, 0.4f, 0.0f)),
    PUNCH(5, R.string.equalizer_default_scheme_name_punch, arrayListOf(0.3f, 0.5f, 0.3f, -0.1f, -0.3f, -0.5f, -0.5f, -0.3f, -0.2f, 0.1f, 0.1f, 0.1f, 0.0f, 0.2f, 0.1f, 0.3f, 0.5f, 0.3f)),
    PRESENCE(6, R.string.equalizer_default_scheme_name_presence, arrayListOf(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.3f, 0.5f, 0.4f, 0.3f, 0.2f, 0.0f, 0.0f, 0.0f, 0.0f)),
    PREMASTER(7, R.string.equalizer_default_scheme_name_premaster, arrayListOf(0.0f, 0.1f, 0.3f, 0.0f, -0.3f, -0.3f, 0.0f, 0.0f, 0.0f, 0.2f, 0.0f, 0.0f, 0.3f, 0.0f, 0.3f, 0.1f, 0.3f, 0.2f)),
    POP(8, R.string.equalizer_default_scheme_name_pop, arrayListOf(0.6f, 0.5f, 0.3f, 0.0f, -0.2f, -0.4f, -0.4f, -0.6f, -0.3f, 0.1f, 0.0f, 0.0f, 0.2f, 0.1f, 0.2f, 0.4f, 0.5f, 0.6f)),
    METAL(9, R.string.equalizer_default_scheme_name_metal, arrayListOf(0.4f, 0.5f, 0.5f, 0.3f, 0.0f, -0.1f, -0.2f, -0.1f, 0.0f, 0.1f, 0.1f, 0.1f, 0.1f, 0.0f, -0.1f, -0.1f, -0.1f, -0.1f)),
    LOUDNESS(10, R.string.equalizer_default_scheme_name_loudness, arrayListOf(0.4f, 0.4f, 0.4f, 0.2f, -0.2f, -0.2f, -0.2f, -0.2f, -0.2f, -0.2f, -0.2f, -0.4f, -1f, -0.7f, 0.0f, 0.3f, 0.4f, 0.4f)),
    HOME_THEATRE(11, R.string.equalizer_default_scheme_name_home_theatre, arrayListOf(0.5f, 0.2f, 0.0f, -0.2f, -0.3f, -0.5f, -0.6f, -0.6f, -0.5f, -0.2f, -0.1f, 0.0f, -0.1f, -0.3f, 0.3f, 0.4f, 0.3f, 0.0f)),
    DRUMS(12, R.string.equalizer_default_scheme_name_drums, arrayListOf(0.2f, 0.1f, 0.0f, 0.0f, 0.0f, -0.2f, 0.0f, -0.2f, 0.0f, 0.0f, 0.0f, 0.0f, 0.2f, 0.0f, 0.0f, 0.3f, 0.0f, 0.0f)),
    DARK(13, R.string.equalizer_default_scheme_name_dark, arrayListOf(-0.3f, -0.1f, -0.1f, -0.1f, -0.1f, -0.1f, -0.1f, -0.1f, -0.1f, -0.1f, -0.1f, -0.3f, -0.4f, -0.5f, -0.6f, -0.7f, -0.9f, -0.9f)),
    CLEAR(14, R.string.equalizer_default_scheme_name_clear, arrayListOf(0.1f, 0.1f, 0.0f, 0.0f, 0.0f, -0.3f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.2f, 0.2f, 0.2f, 0.1f)),
    CLASSIS(15, R.string.equalizer_default_scheme_name_classic, arrayListOf(0.5f, 0.2f, 0.0f, -0.2f, -0.5f, -0.6f, -0.8f, -0.8f, -0.7f, -0.7f, -0.4f, -0.3f, -0.1f, 0.1f, 0.3f, 0.5f, 0.5f, 0.4f)),
    CAR(16, R.string.equalizer_default_scheme_name_car, arrayListOf(-0.5f, 0.0f, 0.1f, 0.0f, 0.0f, -0.4f, -0.4f, -0.5f, -0.5f, -0.5f, -0.3f, -0.2f, -0.2f, 0.0f, 0.1f, 0.0f, -0.2f, -0.5f)),
    BRITTLE(17, R.string.equalizer_default_scheme_name_brittle, arrayListOf(-0.6f, -0.5f, -0.5f, -0.4f, -0.3f, -0.3f, -0.3f, -0.2f, -0.1f, -0.1f, -0.1f, -0.1f, -0.0f, 0.1f, 0.2f, 0.2f, 0.1f, 0.0f)),
    AIR(18, R.string.equalizer_default_scheme_name_air, arrayListOf(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.3f, 0.2f));


    fun getScheme(context: Context, bandsCount: Short): EqualizerScheme {
        val name = context.getString(nameResId)
        val values = getNormalizedValues(bandsCount, values).toMutableList()

        return EqualizerScheme(
                id = id,
                editable = false,
                name = name,
                values = values
        )
    }

    companion object {

        val DEFAULT = NORMAL

        val MAX_ID = (values().maxBy { it.id } ?: DEFAULT).id

        private val EQUALS_FLOAT_D = 0.0001f

        private fun getNormalizedValues(bandsCount: Short, values: List<Float>): List<Float> {
            if (bandsCount < 2) {
                return (0 until bandsCount).map { 0f }
            }

            val valuesCount = values.size.toShort()
            if (valuesCount == bandsCount) {
                return values.toList()
            }

            val scaleFactor = (valuesCount - 1).toFloat() / (bandsCount - 1).toFloat()
            return (0 until bandsCount).map { calcInterpolation(it, bandsCount, values, scaleFactor) }
        }

        private fun calcInterpolation(band: Int, bandsCount: Short, values: List<Float>, scaleFactor: Float): Float {
            if (band <= 0) {
                return values.first()
            }

            if (band >= bandsCount - 1) {
                return values.last()
            }

            val scaledPos = band * scaleFactor
            val scaledPosMin = Math.floor(scaledPos.toDouble()).toInt()
            val scaledPosMinValue = values[scaledPosMin]
            if (scaledPos - scaledPosMin < EQUALS_FLOAT_D) {
                return scaledPosMinValue
            }

            val scaledPosMax = scaledPosMin + 1
            val scaledPosMaxValue = values[scaledPosMax]
            if (scaledPosMax - scaledPos < EQUALS_FLOAT_D) {
                return scaledPosMaxValue
            }

            val minToMaxPercentage = scaledPos - scaledPosMin
            return scaledPosMinValue + (scaledPosMaxValue - scaledPosMinValue) * minToMaxPercentage
        }

    }

}