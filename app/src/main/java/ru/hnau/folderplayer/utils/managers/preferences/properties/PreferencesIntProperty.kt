package ru.hnau.folderplayer.utils.managers.preferences.properties

import android.content.Context
import android.content.SharedPreferences

/**
 * Свойство типа Long
 */
class PreferencesIntProperty(key: String, defaultValue: Int = 0) : PreferencesProperty<Int>(key, defaultValue) {

    override fun readValue(container: SharedPreferences, key: String) = container.getInt(key, defaultValue)

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: Int) {
        editor.putInt(key, value)
    }
}