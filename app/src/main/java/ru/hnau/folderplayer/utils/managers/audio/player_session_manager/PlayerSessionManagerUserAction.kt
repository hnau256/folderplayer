package ru.hnau.folderplayer.utils.managers.audio.player_session_manager

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import ru.hnau.folderplayer.activity.AppActivity
import ru.hnau.folderplayer.utils.Utils

enum class PlayerSessionManagerUserAction(val executor: () -> Unit) {
    PREV(PlayerSessionManager::prev),
    PAUSE_OR_RESUME({ PlayerSessionManager.pauseOrResume() }),
    NEXT(PlayerSessionManager::next),
    STOP(PlayerSessionManager::stop),
    SHOW_ACTIVITY({});

    companion object {

        private val INTENT_EXTRA_KEY = "Action"

        private var showActivityIntent: Intent? = null

        private fun getByName(name: String) = values().find { it.name == name }

        fun exeForIntent(intent: Intent?) {
            val name = intent?.getStringExtra(INTENT_EXTRA_KEY) ?: return
            val action = getByName(name) ?: return
            action.executor.invoke()
        }

        fun getShowActivityIntent(context: Context): Intent {
            var showActivityIntent = showActivityIntent
            if (showActivityIntent == null) {
                showActivityIntent = Intent(context, AppActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT)!!
                Companion.showActivityIntent = showActivityIntent
            }
            return showActivityIntent
        }

    }

    fun getPendingIntent(context: Context): PendingIntent {
        val intent = Intent(context, PlayerSessionManagerActionReceiver::class.java).apply {
            putExtra(INTENT_EXTRA_KEY, name)
        }
        return PendingIntent.getBroadcast(context, Utils.generateId(), intent, 0)
    }

    fun addToView(context: Context, remoteViews: RemoteViews, buttonId: Int) {
        if (this == SHOW_ACTIVITY) {
            val showActivityIntent = getShowActivityIntent(context)
            remoteViews.setOnClickPendingIntent(buttonId, PendingIntent.getActivity(context, Utils.generateId(), showActivityIntent, 0))
            return
        }
        remoteViews.setOnClickPendingIntent(buttonId, getPendingIntent(context))
    }

}