package ru.hnau.folderplayer.utils.ui

import ru.hnau.folderplayer.utils.listeners_containers.ListenersDataContainer
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager


object ScaleManager {

    private val DEFAULT_ID = 4

    val values = hashMapOf(
            1 to 0.5f,
            2 to 0.75f,
            3 to 0.9f,
            4 to 1f,
            5 to 1.124f,
            6 to 1.25f,
            7 to 1.50f,
            8 to 2f
    )

    var id: Int
        set(newId) {
            val normalizedId = normalizeId(newId)
            PreferencesManager.uxScaleId = normalizedId
            onIdChangedListenersContainer.call(normalizedId)
        }
        get() = getPreferencesIdOrDefault()

    val ids = values.keys.toSortedSet().toList()

    val value: Float
        get() = values[id] ?: 1f

    private val onIdChangedListenersContainer = ListenersDataContainer<Int>()

    private fun getPreferencesIdOrDefault() = normalizeId(PreferencesManager.uxScaleId)

    private fun normalizeId(id: Int): Int {
        if (values.containsKey(id)) {
            return id
        }
        return DEFAULT_ID
    }


}