package ru.hnau.folderplayer.utils.managers.preferences.properties

import android.content.Context
import android.content.SharedPreferences

/**
 * Свойство типа множества строк
 */
class PreferencesStringSetProperty(key: String, defaultValue: MutableSet<String> = HashSet<String>()) : PreferencesProperty<MutableSet<String>>(key, defaultValue) {

    override fun readValue(container: SharedPreferences, key: String): MutableSet<String> = container.getStringSet(key, defaultValue)!!

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: MutableSet<String>) {
        editor.putStringSet(key, value)
    }

    override fun valueIsMutable() = true
}