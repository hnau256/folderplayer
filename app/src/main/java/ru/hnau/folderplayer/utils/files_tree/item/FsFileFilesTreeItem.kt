package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.managers.audio.AudioFormat
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import java.io.File


class FsFileFilesTreeItem private constructor(
        context: Context,
        path: String,
        val audio: Boolean,
        file: File
) : FsFilesTreeItem(
        context,
        path,
        DrawableGetter(if (audio) R.drawable.ic_files_tree_item_audio else R.drawable.ic_files_tree_item_file),
        file
) {

    companion object {

        fun create(context: Context, path: String, file: File = File(path)): FsFileFilesTreeItem {
            val audio = getIsAudio(path)
            return FsFileFilesTreeItem(context, path, audio, file)
        }

        private fun getIsAudio(path: String): Boolean {
            val ext = extFromPath(path) ?: return false
            return AudioFormat.formats.contains(ext)
        }

        private fun extFromPath(path: String): String? {
            val length = path.length
            val dotPos = path.lastIndexOf('.')
            if (dotPos < 0 || dotPos > length-2) {
                return null
            }
            return path.substring(dotPos + 1).toLowerCase()
        }

    }

}