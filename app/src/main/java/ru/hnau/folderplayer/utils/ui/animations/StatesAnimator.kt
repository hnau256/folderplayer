package ru.hnau.folderplayer.utils.ui.animations

/**
 * Плавный переключатель между несколькими состояниями
 */
class StatesAnimator(
        private val statesCount: Int,
        private val stepListener: (Float) -> Unit,
        private val neighboringStatesAnimationTime: Long = NEIGHBORING_STATES_DEFAULT_ANIMATION_TIME) {

    companion object {
        val NEIGHBORING_STATES_DEFAULT_ANIMATION_TIME = 100L

        private val COMPARISON_SENSITIVITY = 0.01f
    }

    private var animateToTarget = false
    private var animationTarget: Int = 0
    private var animating = false
    var target: Int = 0
        private set

    var current: Double = 0.0
        private set(value) {
            field = value
            stepListener.invoke(value.toFloat())
        }

    private val animationStep = { onTimerStep() }

    private var startTime = 0.0
    private var endTime = 0.0
    private var from = 0.0

    init {
        if (statesCount < 1) {
            throw IllegalArgumentException("States count ($statesCount) < 1")
        }
    }

    fun addTarget(state: Int, animate: Boolean) {
        target = if (state < 0) 0 else if (state >= statesCount) statesCount - 1 else state
        animateToTarget = animate && AnimationManager.canUse(AnimationManager.Type.SIMPLE)
        startAnimation()
    }

    fun startAnimation() {
        synchronized(this, {
            if (animating) {
                return
            }
            animating = true
        })
        animationTarget = target

        if (!animateToTarget) {
            current = target.toDouble()
        }

        if (currentIsTarget()) {
            animating = false
            return
        }

        doAnimation(current)
    }

    fun doAnimation(from: Double) {
        this.from = from
        startTime = System.currentTimeMillis().toDouble()
        endTime = startTime + (Math.abs(from - target) * neighboringStatesAnimationTime)
        AnimatorsMetronome.addListener(animationStep)
    }

    private fun onTimerStep() {
        if (animationTarget != target) {
            doNextAnimation()
            return
        }
        val currentTime = System.currentTimeMillis().toDouble()
        val percentage = (currentTime - startTime) / (endTime - startTime)
        val end = percentage >= 1.0
        current = if (end) target.toDouble() else from + (target - from) * percentage
        if (end) {
            doNextAnimation()
        }
    }

    private fun doNextAnimation() {
        animating = false
        AnimatorsMetronome.removeListener(animationStep)
        startAnimation()
    }

    fun currentIsTarget() = Math.abs(current - target) < COMPARISON_SENSITIVITY

}