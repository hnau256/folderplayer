package ru.hnau.folderplayer.utils.permission

import android.Manifest


enum class PermissionType(val androidValue: String) {

    WRITE_EXTERNAL_STORAGE(Manifest.permission.WRITE_EXTERNAL_STORAGE);

}