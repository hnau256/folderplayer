package ru.hnau.folderplayer.utils.managers

import android.util.Log

@Suppress("ConstantConditionIf")
object Logger {

    private val USE_LOGGING = true
    private val TAG_PREFIX = "FolderplayerLogs_"

    fun d(tag: String, message: String, throwable: Throwable) {
        if (USE_LOGGING) {
            val msg = throwable.message ?: ""
            if (msg.isNotBlank()) {
                Log.d(TAG_PREFIX + tag, message + ", " + msg)
                return
            }
            Log.d(TAG_PREFIX + tag, message, throwable)
        }
    }

    fun e(tag: String, message: String, throwable: Throwable) {
        if (USE_LOGGING) {
            val msg = throwable.message ?: ""
            if (msg.isNotBlank()) {
                Log.e(TAG_PREFIX + tag, message + ", " + msg)
                return
            }
            Log.e(TAG_PREFIX + tag, message, throwable)
        }
    }

    fun w(tag: String, message: String, throwable: Throwable) {
        if (USE_LOGGING) {
            val msg = throwable.message ?: ""
            if (msg.isNotBlank()) {
                Log.w(TAG_PREFIX + tag, message + ", " + msg)
                return
            }
            Log.w(TAG_PREFIX + tag, message, throwable)
        }
    }

    fun d(tag: String, message: String) {
        if (USE_LOGGING) {
            Log.d(TAG_PREFIX + tag, message)
        }
    }

    fun e(tag: String, message: String) {
        if (USE_LOGGING) {
            Log.e(TAG_PREFIX + tag, message)
        }
    }

    fun w(tag: String, message: String) {
        if (USE_LOGGING) {
            Log.w(TAG_PREFIX + tag, message)
        }
    }

}
