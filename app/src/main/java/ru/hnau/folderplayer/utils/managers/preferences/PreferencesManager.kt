package ru.hnau.folderplayer.utils.managers.preferences

import android.annotation.SuppressLint
import ru.hnau.folderplayer.utils.managers.preferences.properties.*

/**
 * Хранение значений в постоянной памяти
 */
@SuppressLint("StaticFieldLeak")
object PreferencesManager {

    var repeatType: Int by PreferencesIntProperty("PLAY_SETTINGS_REPEAT_TYPE")
    var filesExplorerViewPath: String by PreferencesStringProperty("FILES_EXPLORER_VIEW_PATH")
    var equalizerSchemeId: Long by PreferencesLongProperty("EQUALIZER_SCHEME_ID")
    var resumeWhereStoppedFilePath: String by PreferencesStringProperty("RESUME_WHERE_STOPPED_FILE_PATH")
    var resumeWhereStoppedSeconds: Int by PreferencesIntProperty("RESUME_WHERE_STOPPED_SECONDS")

    //Settings
    var goToUpDirOnBackClicked: Boolean by PreferencesBooleanProperty("GO_TO_UP_DIR_ON_BACK_CLICKED", true)
    var uxScaleId: Int by PreferencesIntProperty("UX_SCALE_ID")
    var pauseOnHeadphonesUnplugged: Boolean by PreferencesBooleanProperty("PAUSE_ON_HEADPHONES_UNPLUGGED", true)
    var resumeOnHeadphonesPlugged: Boolean by PreferencesBooleanProperty("RESUME_ON_HEADPHONES_PLUGGED", true)
    var pauseOnUnfocused: Boolean by PreferencesBooleanProperty("PAUSE_ON_UNFOCUSED", true)
    var resumeOnFocused: Boolean by PreferencesBooleanProperty("RESUME_ON_FOCUSED", true)
    var resumeWhereStopped: Boolean by PreferencesBooleanProperty("RESUME_WHERE_STOPPED", true)
    var resumeWhereStoppedInactiveWidget: Boolean by PreferencesBooleanProperty("RESUME_WHERE_STOPPED_INACTIVE_WIDGET", true)
    var pauseBetweenFiles: Boolean by PreferencesBooleanProperty("PAUSE_BETWEEN_FILES", false)
}