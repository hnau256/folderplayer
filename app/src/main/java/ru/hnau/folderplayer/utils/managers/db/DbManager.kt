package ru.hnau.folderplayer.utils.managers.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.Logger


object DbManager {

    private val TAG = DbManager.javaClass.simpleName
    private val DB_NAME = "folderplayer"
    private val DB_VERSION = 1

    private var db: SQLiteDatabase? = null

    fun getDb(context: Context): SQLiteDatabase? {
        var db = db
        if (db == null) {
            db = openDb(context) ?: return null
            DbManager.db = db
        }
        return db
    }

    private fun openDb(context: Context) = Utils.tryOrElse({
        openDbUnsafe(context)
    }, {
        Logger.d(TAG, "Error while opening DB '$DB_NAME'", it)
        FirebaseManager.sendThrowable(it)
        null
    })

    private fun openDbUnsafe(context: Context): SQLiteDatabase? {
        val openHelper = object : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

            private var updated = false

            override fun onCreate(db: SQLiteDatabase) {
                DbUpdater.applyUpdates(db, DB_VERSION)
                updated = true
            }

            override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
                if (!updated) {
                    DbUpdater.applyUpdates(db, DB_VERSION)
                    updated = true
                }
            }

            override fun onOpen(db: SQLiteDatabase) {
                super.onOpen(db)
                if (!updated) {
                DbUpdater.applyUpdates(db, DB_VERSION)
                    updated = true
                }
            }

        }
        return openHelper.writableDatabase
    }


}