package ru.hnau.folderplayer.utils.managers.audio.player_session_manager

import android.app.Activity
import android.content.Context
import android.content.Intent
import ru.hnau.folderplayer.utils.UriToPathConverter
import ru.hnau.folderplayer.utils.files_tree.item.FsFileFilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.FsFilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.OpenableFilesTreeItem
import ru.hnau.folderplayer.utils.managers.AppActivityManager
import ru.hnau.folderplayer.utils.managers.audio.PlayItem
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager
import ru.hnau.folderplayer.utils.permission.PermissionRequester
import ru.hnau.folderplayer.utils.permission.PermissionType


object PlayerSessionManagerStarter {


    fun onAudioClicked(item: FsFileFilesTreeItem) {
        if (!item.audio) {
            return
        }

        val playItem = PlayItem(item.path)

        val parent = item.getParent() as? OpenableFilesTreeItem ?: return
        val playItems = parent.getSubitems().sorted().mapNotNull {
            if (it is FsFileFilesTreeItem && it.audio) {
                PlayItem(it.path)
            } else {
                null
            }
        }

        PlayerSessionManager.playSession(playItem, playItems)
    }

    fun onIntentReceived(activity: Activity, intent: Intent?) {
        intent ?: return
        onIntentReceivedInner(activity, intent)
    }

    private fun onIntentReceivedInner(activity: Activity, intent: Intent) {
        if (intent.action != Intent.ACTION_VIEW) {
            return
        }
        val uri = intent.data ?: return
        val path = UriToPathConverter.uriToPath(activity, uri) ?: return

        PermissionRequester.requestPermission(activity, PermissionType.WRITE_EXTERNAL_STORAGE, {
            onPathFromIntentReceived(activity, path)
        })
    }

    private fun onPathFromIntentReceived(context: Context, path: String) {
        val fileTreeItem = FsFileFilesTreeItem.create(context, path)
        PlayerSessionManagerStarter.onAudioClicked(fileTreeItem)

        val fileParent = fileTreeItem.getParent()
        val parentPath = (fileParent as? FsFilesTreeItem)?.path
        parentPath?.let { PreferencesManager.filesExplorerViewPath = it }
    }

}