package ru.hnau.folderplayer.utils.ui.animations

import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import ru.hnau.folderplayer.utils.managers.Logger
import java.util.concurrent.CopyOnWriteArraySet

object Animator {

    private val TAG = Animator::class.java.simpleName
    private val DEBUG_LOG = false
    private val DEBUG_LOG_LENGTH = 30

    private data class Animation(val duration: Float,
                                 val start: Long,
                                 val end: Long,
                                 val iterator: (Float) -> Unit,
                                 val interpolator: Interpolator = LinearInterpolator(),
                                 val endListener: (() -> Unit)? = null)

    private val animations = CopyOnWriteArraySet<Animation>()

    private val animationStep = { doAnimationsStep() }

    fun doAnimation(
            duration: Long,
            iterator: (Float) -> Unit,
            interpolator: Interpolator = LinearInterpolator(),
            endListener: (() -> Unit)? = null) {

        if (!AnimationManager.canUse(AnimationManager.Type.SIMPLE) || duration <= 0) {
            iterator.invoke(1f)
            endListener?.invoke()
            return
        }

        val start = System.currentTimeMillis()
        val end = start + duration
        addAnimation(Animation(duration.toFloat(), start, end, iterator, interpolator, endListener))
    }

    private fun doAnimationsStep() {
        val now = System.currentTimeMillis()
        animations.forEach { doAnimationStep(it, now) }
    }

    private fun doAnimationStep(animation: Animation, now: Long) {
        if (animation.end <= now) {
            removeAnimation(animation)
            animation.iterator.invoke(1f)
            animation.endListener?.invoke()
            return
        }
        val percentage = (now - animation.start) / animation.duration
        animation.iterator.invoke(animation.interpolator.getInterpolation(percentage))
        printDebugLog(percentage)
    }

    private fun addAnimation(animation: Animation) = synchronized(this, {
        animations.add(animation)
        if (!animations.isEmpty()) {
            AnimatorsMetronome.addListener(animationStep)
        }
    })

    private fun removeAnimation(animation: Animation) = synchronized(this, {
        animations.remove(animation)
        if (animations.isEmpty()) {
            AnimatorsMetronome.removeListener(animationStep)
        }
    })

    private fun printDebugLog(percentage: Float) {
        if (!DEBUG_LOG) {
            return
        }
        val maxProgress = DEBUG_LOG_LENGTH
        val progress = (maxProgress * percentage).toInt()
        val msg = String(kotlin.CharArray(progress, { ' ' })) + "#"
        Logger.d(TAG, msg)
    }

}