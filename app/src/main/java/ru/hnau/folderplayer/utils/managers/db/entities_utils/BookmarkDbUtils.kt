package ru.hnau.folderplayer.utils.managers.db.entities_utils

import android.content.Context
import android.database.Cursor
import ru.hnau.folderplayer.utils.managers.bookmarks.Bookmark
import ru.hnau.folderplayer.utils.managers.db.DbUtils


object BookmarkDbUtils {

    private val TABLE_NAME = "BOOKMARK"

    private val FIELD_NAME_ID = "ID"
    private val FIELD_NAME_PATH = "PATH"

    private val constructorFromCursor = { cursor: Cursor ->
        Bookmark(
                id = cursor.getLong(cursor.getColumnIndex(FIELD_NAME_ID)),
                path = cursor.getString(cursor.getColumnIndex(FIELD_NAME_PATH))
        )
    }

    fun getAll(context: Context?) = context?.let {
        DbUtils.queryList(
                context = it,
                table = TABLE_NAME,
                entityConstrictor = constructorFromCursor
        )
    } ?: emptyList()

    fun insert(context: Context?, bookmark: Bookmark) = context?.let {
        DbUtils.insert(it, TABLE_NAME, hashMapOf(
                Pair(FIELD_NAME_ID, bookmark.id),
                Pair(FIELD_NAME_PATH, bookmark.path)
        ))
    }

    fun remove(context: Context?, id: Long) = context?.let {
        DbUtils.removeWithSimpleWhere(it, TABLE_NAME, FIELD_NAME_ID, id)
    }

}