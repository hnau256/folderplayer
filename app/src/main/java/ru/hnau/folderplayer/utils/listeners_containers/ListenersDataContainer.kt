package ru.hnau.folderplayer.utils.listeners_containers

import java.util.concurrent.CopyOnWriteArraySet

/**
 * Добавление, удаление и вызов слушателей
 */
class ListenersDataContainer<T>(
        private val onFirstAdded: (() -> Unit)? = null,
        private val onLastRemoved: (() -> Unit)? = null
) {

    private val listeners = CopyOnWriteArraySet<(T) -> Unit>()

    val count: Int
        get() = listeners.size

    fun add(listener: (T) -> Unit) {
        synchronized(this) {
            val oldCount = count
            listeners.add(listener)
            if (oldCount == 0 && count == 1) {
                onFirstAdded?.invoke()
            }
        }
    }

    fun remove(listener: (T) -> Unit) {
        synchronized(this) {
            val oldCount = count
            listeners.remove(listener)
            if (oldCount == 1 && count == 0) {
                onLastRemoved?.invoke()
            }
        }
    }

    fun call(data: T) = listeners.forEach { it.invoke(data) }

}