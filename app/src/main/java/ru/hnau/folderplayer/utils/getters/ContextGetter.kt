package ru.hnau.folderplayer.utils.getters

import android.content.Context


abstract class ContextGetter<T>(data: T? = null) {

    private var cached: T? = data

    protected fun set(data: T) {
        cached = data
    }

    fun get(context: Context): T {
        var cached = this.cached
        if (cached == null) {
            cached = generate(context)
            this.cached = cached
        }
        return cached!!
    }

    protected abstract fun generate(context: Context): T

}