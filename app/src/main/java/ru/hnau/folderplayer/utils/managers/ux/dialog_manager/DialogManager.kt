package ru.hnau.folderplayer.utils.managers.ux.dialog_manager

import ru.hnau.folderplayer.activity.view.bottom_animator.BottomAnimatorView
import ru.hnau.folderplayer.utils.managers.AppActivityManager
import ru.hnau.folderplayer.utils.ui.dpToPxInt


object DialogManager {

    val PADDING_V: Int
        get() = dpToPxInt(16)

    val PADDING_H: Int
        get() = dpToPxInt(24)

    fun show(constructor: DialogBuilder.() -> Unit) {
        val context = AppActivityManager.activity ?: return
        val dialogBuilder = DialogBuilder(context)
        constructor.invoke(dialogBuilder)
        val dialogView = DialogView(dialogBuilder)
        val bottomView = BottomAnimatorView(
                view = dialogView,
                goBackHandler = { !dialogBuilder.cancellable },
                onHideCallback = dialogBuilder.onCancelledCallback
        )
        AppActivityManager.showBottomView(bottomView)
    }

    fun close() = AppActivityManager.hideBottomView()

}