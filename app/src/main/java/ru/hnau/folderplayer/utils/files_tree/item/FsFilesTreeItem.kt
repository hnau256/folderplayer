package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.audio.PlayItem
import java.io.File

open class FsFilesTreeItem(
        context: Context,
        val path: String,
        icon: DrawableGetter,
        val file: File = File(path)
) : FilesTreeItem(
        context,
        icon
) {

    override fun getParent() = createByPath(context, file.parent) as? OpenableFilesTreeItem ?: RootFilesTreeItem(context)

    override fun getTitle() = StringGetter(file.name)

    override fun getPlayItemRelation(playItem: PlayItem) = PlayItemRelationChecker.getPathPlayItemRelation(path, playItem)

    companion object {

        fun createByPath(context: Context, path: String?): FsFilesTreeItem? {
            if (path == null) {
                return null
            }

            val file = File(path)
            if (!file.exists() || !file.canRead()) {
                return null
            }
            if (file.isDirectory) {
                return FsDirFilesTreeItem(context, path = path, file = file)
            }

            return FsFileFilesTreeItem.create(context, path, file)

        }

        fun getFileSubitems(context: Context, file: File): List<FsFilesTreeItem> {
            val subitems = file.listFiles() ?: return emptyList()
            return subitems.mapNotNull { FsFilesTreeItem.createByPath(context, it.absolutePath) }
        }

    }
}