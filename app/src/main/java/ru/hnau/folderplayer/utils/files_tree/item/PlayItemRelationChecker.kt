package ru.hnau.folderplayer.utils.files_tree.item

import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.managers.audio.PlayItem


interface PlayItemRelationChecker {

    enum class Type(val fgColor: Int, val bgColor: Int) {
        NONE(fgColor = ColorGetter.FG, bgColor = ColorGetter.BG),
        EQUALS(fgColor = ColorGetter.PRIMARY, bgColor = ColorGetter.SELECT),
        PARENT(fgColor = ColorGetter.PRIMARY, bgColor = ColorGetter.BG)
    }

    fun getPlayItemRelation(playItem: PlayItem): Type

    companion object {

        fun getPathPlayItemRelation(path: String, playItem: PlayItem): Type {
            val playItemPath = playItem.path
            if (path == playItemPath) {
                return Type.EQUALS
            }
            if (playItemPath.startsWith(path)) {
                return Type.PARENT
            }
            return Type.NONE
        }

    }
}