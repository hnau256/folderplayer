package ru.hnau.folderplayer.utils.managers.audio.player_session_manager

import android.content.Intent
import android.os.Build
import android.os.Handler
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.service.FgService
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.ContextContainer
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.audio.*
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.DialogManager


object PlayerSessionManager {

    private const val MAX_ATTEMPTS_TO_PLAY_NEXT = 10
    private const val PAUSE_BETWEEN_FILES = 3 * 1000L // 3 sec

    private var shuffledCurrentItems: List<PlayItem>? = null

    private var currentItems: List<PlayItem>? = null
        set(value) {
            field = value
            shuffledCurrentItems = value?.shuffled()
        }

    private var currentItem: PlayItem? = null

    init {
        PlayerManager.addOnFinishedListener { switchToNextOrPrev(true, true) }
    }

    fun playSession(firstItem: PlayItem, items: List<PlayItem>) {
        FirebaseManager.sendEvent("Player session started")
        synchronized(this, {
            currentItems = items
            if (!PlayerManager.start(firstItem, true)) {
                unableToPlayItem(firstItem)
                return@synchronized
            }
            currentItem = firstItem
            startService()
        })
    }

    fun next() = switchToNextOrPrev(true, false)

    fun prev() = switchToNextOrPrev(false, false)


    fun pauseOrResume() {
        val pausedOrResumed = PlayerManager.pauseOrResume(PlayerManagerPauseType.USER)
        if (pausedOrResumed) {
            return
        }
        ResumeWhereStoppedManager.tryResumeWhereStopped()
    }

    fun stop() = PlayerManager.stop()

    private fun switchToNextOrPrev(next: Boolean, usePauseBetweenFilesIfNeed: Boolean, attemptsCount: Int = 0) {
        val currentItem = currentItem
        val items = currentItems
        val shuffledItems = shuffledCurrentItems
        if (currentItem == null || items == null || shuffledItems == null) {
            return
        }
        val delta = if (next) 1 else -1
        val nextItem = RepeatManager.getNextItem(currentItem, items, shuffledItems, delta) ?: return

        if (usePauseBetweenFilesIfNeed && PreferencesManager.pauseBetweenFiles) {
            Handler().postDelayed({ startNewItem(next, nextItem, attemptsCount) }, PAUSE_BETWEEN_FILES)
            return
        }

        startNewItem(next, nextItem, attemptsCount)
    }

    private fun startNewItem(next: Boolean, item: PlayItem, attemptsCount: Int) {
        PlayerSessionManager.currentItem = item
        if (PlayerManager.start(item, false)) {
            return
        }
        if (attemptsCount > MAX_ATTEMPTS_TO_PLAY_NEXT) {
            unableToPlayItem(item)
            return
        }
        switchToNextOrPrev(next, false, attemptsCount + 1)
    }

    private fun startService() {
        val context = ContextContainer.context
        val intent = Intent(context, FgService::class.java)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            context.startService(intent)
        } else {
            context.startForegroundService(intent)
        }
    }

    private fun unableToPlayItem(item: PlayItem) {
        DialogManager.show {
            val errorMsgPrefix = context.getString(R.string.player_manager_error_play_item)
            content(StringGetter(errorMsgPrefix + " " + item.filename))
        }
    }


}