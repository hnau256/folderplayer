package ru.hnau.folderplayer.utils.managers.preferences.properties

import android.content.Context
import android.content.SharedPreferences

/**
 * Свойство типа Long
 */
class PreferencesFloatProperty(key: String, defaultValue: Float = 0f) : PreferencesProperty<Float>(key, defaultValue) {

    override fun readValue(container: SharedPreferences, key: String) = container.getFloat(key, defaultValue)

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: Float) {
        editor.putFloat(key, value)
    }
}