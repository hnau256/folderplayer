package ru.hnau.folderplayer.utils.permission

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import ru.hnau.folderplayer.utils.autokey_map_cache.IdAutokeyMapCache
import ru.hnau.folderplayer.utils.managers.FirebaseManager


object PermissionRequester {

    private data class Callbacks(
            val permission: PermissionType,
            val onGranted: () -> Unit,
            val onDenied: () -> Unit
    )

    private val callbacksCache = IdAutokeyMapCache<Callbacks>()

    private const val GRANTED = PackageManager.PERMISSION_GRANTED

    fun requestPermission(activity: Activity, permission: PermissionType, onGranted: () -> Unit, onDenied: () -> Unit = {}) {
        requestPermission(activity, permission, Callbacks(permission, onGranted, onDenied), true)
    }

    fun onRequestResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        val callbacks = callbacksCache.remove(requestCode) ?: return
        val result = grantResults.getOrNull(0) ?: return

        if (result == GRANTED) {
            callbacks.onGranted.invoke()
            sendAnalytics(callbacks.permission, true, "user")
        } else {
            callbacks.onDenied.invoke()
            sendAnalytics(callbacks.permission, false, "user")
        }
    }

    private fun requestPermission(activity: Activity, permission: PermissionType, callbacks: Callbacks, showExplanation: Boolean) {
        if (ContextCompat.checkSelfPermission(activity, permission.androidValue) == GRANTED) {
            callbacks.onGranted.invoke()
            sendAnalytics(permission, true, "default")
            return
        }
/*
        if (showExplanation && ActivityCompat.shouldShowRequestPermissionRationale(activity, permission.androidValue)) {
            showExplanationAndRetry(activity, permission, callbacks)
            return
        }*/

        val requestCode = callbacksCache.put(callbacks)
        if (requestCode == null) {
            sendAnalytics(permission, false, "error while generating id")
            callbacks.onDenied.invoke()
            return
        }
        ActivityCompat.requestPermissions(activity, arrayOf(permission.androidValue), requestCode)
    }

    private fun showExplanationAndRetry(activity: Activity, permission: PermissionType, callbacks: Callbacks) {
        requestPermission(activity, permission, callbacks, false)
    }

    private fun sendAnalytics(permission: PermissionType, granted: Boolean, by: String) =
            FirebaseManager.sendEvent("Permission request result", mapOf(
                    "permission" to permission.androidValue,
                    "granted" to granted.toString(),
                    "by" to by
            ))

}