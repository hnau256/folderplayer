package ru.hnau.folderplayer.utils.ui.animations.ripple_animator

import android.graphics.PointF
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator


class RippleAnimatorCircle(
        private val deltaR: Float,
        lifetime: Long,
        private val positionToTargetInterpolator: Interpolator
) {

    data class Info(
            val pos: PointF,
            val r: Float,
            val alpha: Float,
            val progress: Float
    )

    companion object {
        private val SIZE_INTERPOLATOR = DecelerateInterpolator()
        private val ALPHA_INTERPOLATOR = AccelerateInterpolator()
    }

    private val lifetime = lifetime.toDouble()

    private var initialPos: PointF = PointF()
    private var targetPos: PointF? = null
    private var initialR: Float = 0f
    private var started = 0.0


    val finished: Boolean
        get() = (System.currentTimeMillis().toDouble() - started) > lifetime

    val info: Info?
        get() {
            val now = System.currentTimeMillis().toDouble()
            val progress = ((now - started) / lifetime).toFloat()
            if (progress > 1) {
                return null
            }

            val radiusPercentage = SIZE_INTERPOLATOR.getInterpolation(progress)
            val r = initialR + deltaR * radiusPercentage

            val posProgress = positionToTargetInterpolator.getInterpolation(progress)

            val targetPos = this.targetPos
            val pos = targetPos?.let {
                PointF(
                        initialPos.x + (targetPos.x - initialPos.x) * posProgress,
                        initialPos.y + (targetPos.y - initialPos.y) * posProgress
                )
            } ?: initialPos

            return Info(pos, r, ALPHA_INTERPOLATOR.getInterpolation(1 - progress), progress)
        }

    fun start(initialPos: PointF, initialR: Float, targetPos: PointF?) {
        started = System.currentTimeMillis().toDouble()
        this.initialPos = initialPos
        this.targetPos = targetPos
        this.initialR = initialR
    }


}