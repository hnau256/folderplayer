package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.ContextContainer.context
import java.io.File


class BookmarkFilesTreeItem(
        context: Context,
        val id: Long,
        path: String,
        private val title: StringGetter,
        file: File = File(path)
) : FsDirFilesTreeItem(
        context,
        path,
        DrawableGetter(R.drawable.ic_files_tree_item_bookmark),
        file
) {

    override fun getParent() = BookmarksFilesTreeItem(context)

    override fun getTitle() = title

}