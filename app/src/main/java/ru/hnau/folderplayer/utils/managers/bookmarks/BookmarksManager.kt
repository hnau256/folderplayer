package ru.hnau.folderplayer.utils.managers.bookmarks

import android.content.Context
import ru.hnau.folderplayer.utils.files_tree.item.BookmarkFilesTreeItem
import ru.hnau.folderplayer.utils.getters.StringGetter
import java.io.File


object BookmarksManager {

    private val container = BookmarksContainer()

    fun getBookmarks(context: Context) = container.ids.mapNotNull { id ->
        val path = container.getPath(id)
        val file = File(path)
        if (file.exists()) {
            BookmarkFilesTreeItem(
                    context = context,
                    id = id,
                    title = StringGetter(file.name),
                    path = path,
                    file = file
            )
        } else {
            null
        }
    }

    fun addBookmark(path: String) = container.add(path)

    fun removeBookmark(id: Long) = container.remove(id)

}