package ru.hnau.folderplayer.utils.ui.drawable.layout_drawable

import android.content.Context
import android.graphics.Rect
import android.view.Gravity
import android.view.View
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.preferredHeight
import ru.hnau.folderplayer.utils.ui.preferredWidth


class LayoutDrawableView(
        context: Context,
        initialIcon: DrawableGetter? = null,
        layoutType: LayoutDrawable.LayoutType = LayoutDrawable.LayoutType.PROPORTIONAL_IN,
        gravity: Int = Gravity.CENTER,
        padding: Rect = Rect(),
        scale: Float = 1f
) : View(
        context
) {

    private val backgroundDrawable = LayoutDrawable(
            context = context,
            initialIcon = initialIcon,
            layoutType = layoutType,
            gravity = gravity,
            padding = padding,
            scale = scale
    )

    var icon: DrawableGetter?
        set(value) {
            backgroundDrawable.icon = value
        }
        get() = backgroundDrawable.icon

    init {
        setBackgroundDrawable(backgroundDrawable)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(
                UiUtils.getDefaultSize(icon?.get(context)?.preferredWidth() ?: 0, widthMeasureSpec),
                UiUtils.getDefaultSize(icon?.get(context)?.preferredHeight() ?: 0, heightMeasureSpec)
        )
    }

}