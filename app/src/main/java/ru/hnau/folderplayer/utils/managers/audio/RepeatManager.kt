package ru.hnau.folderplayer.utils.managers.audio

import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.findPos
import ru.hnau.folderplayer.utils.getOrFirst
import ru.hnau.folderplayer.utils.circledGet
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager


object RepeatManager {

    enum class Type(internal val orderNum: Int, val iconResId: Int) {
        LINEAR(orderNum = 1, iconResId = R.drawable.ic_repeat_linear),
        ONE(orderNum = 2, iconResId = R.drawable.ic_repeat_one),
        RANDOM(orderNum = 3, iconResId = R.drawable.ic_repeat_random)
    }

    private val SORTED_TYPES = Type.values().sortedBy { it.orderNum }

    private val DEFAULT_TYPE = SORTED_TYPES[0]

    var type: Type = DEFAULT_TYPE
        private set(value) {
            if (field != value) {
                field = value
                PreferencesManager.repeatType = value.orderNum
            }
        }

    init {
        val typeOrderNumFromPreferences = PreferencesManager.repeatType
        type = SORTED_TYPES.find { it.orderNum == typeOrderNumFromPreferences } ?: DEFAULT_TYPE
    }

    fun switchToNextType(): Type {
        val type = SORTED_TYPES.getOrFirst(type.ordinal + 1)
        this.type = type
        return type
    }

    fun <T> getNextItem(current: T?, items: List<T>, shuffledItems: List<T>, delta: Int = 1): T? {
        current ?: return null
        return when (type) {
            Type.LINEAR -> getNextItem(current, items, delta)
            Type.ONE -> current
            Type.RANDOM -> getNextItem(current, shuffledItems, delta)
        }
    }

    private fun <T> getNextItem(current: T, items: List<T>, delta: Int): T? {
        val pos = items.findPos { it == current } ?: return null
        return items.circledGet(pos + delta)
    }

}