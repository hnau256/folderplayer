package ru.hnau.folderplayer.utils.managers.preferences.properties

import android.content.Context
import android.content.SharedPreferences

/**
 * Свойство строкового типа
 */
class PreferencesStringProperty(key: String, defaultValue: String = "") : PreferencesProperty<String>(key, defaultValue) {

    override fun readValue(container: SharedPreferences, key: String) = container.getString(key, defaultValue)!!

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: String) {
        editor.putString(key, value)
    }
}