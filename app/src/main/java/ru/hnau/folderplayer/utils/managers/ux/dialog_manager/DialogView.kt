package ru.hnau.folderplayer.utils.managers.ux.dialog_manager

import android.view.View
import android.widget.LinearLayout
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.ui.UiUtils

class DialogView(dialogBuilder: DialogBuilder) : LinearLayout(dialogBuilder.context) {

    private val titleView = dialogBuilder.titleView?.apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, UiUtils.WRAP_CONTENT)
    }

    private val contentView = dialogBuilder.contentView?.apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, UiUtils.WRAP_CONTENT)
        if (titleView == null) {
            setPadding(paddingLeft, DialogManager.PADDING_V, paddingRight, paddingBottom)
        }
    }

    private val buttons = createButtonsView(dialogBuilder)?.apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, UiUtils.WRAP_CONTENT)
    }

    init {
        orientation = VERTICAL
        setBackgroundColor(ColorGetter.BG_DARK)

        titleView?.let { addView(it) }
        contentView?.let { addView(it) }
        buttons?.let { addView(it) }
    }

    private fun createButtonsView(dialogBuilder: DialogBuilder): View? {
        if (dialogBuilder.buttons.isEmpty()) {
            return null
        }

        val vertical = dialogBuilder.verticalButtons
        return LinearLayout(context).apply {
            orientation = if (vertical) VERTICAL else HORIZONTAL
            dialogBuilder.buttons.forEachIndexed { i, button ->

                button.layoutParams = LinearLayout.LayoutParams(
                        if (vertical) UiUtils.MATCH_PARENT else 0,
                        if (vertical) UiUtils.WRAP_CONTENT else UiUtils.MATCH_PARENT,
                        1f
                )

                addView(button)
            }
        }
    }

}