package ru.hnau.folderplayer.utils.managers.ux.dialog_manager.actions

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.text.TextUtils
import android.view.Gravity
import android.view.MotionEvent
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.DialogManager
import ru.hnau.folderplayer.utils.ui.animations.ripple_animator.RippleAnimator


class DialogActionView(context: Context) : Label(
        context = context,
        size = SizeGetter(17),
        color = ColorGetter.FG,
        gravity = Gravity.CENTER_VERTICAL or Gravity.LEFT,
        maxLines = 1,
        minLines = 1,
        ellipsize = TextUtils.TruncateAt.END,
        text = StringGetter("Title action")
) {

    private val RIPPLE_COLOR = ColorGetter.BG_DARK

    var action: DialogAction? = null
        set(value) {
            if (field != value) {
                field = value
                text = value?.title?.get(context) ?: ""
            }
        }

    private val rippleAnimator = RippleAnimator(
            context = context,
            onNeedRefresh = this::invalidate,
            maxBgPercentage = 0.3f
    )

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        setOnClickListener { action?.execute() }
        setPadding(DialogManager.PADDING_H, DialogManager.PADDING_V, DialogManager.PADDING_H, DialogManager.PADDING_V)
    }

    override fun draw(canvas: Canvas) {

        val fullRect = Rect(0, 0, width, height)

        paint.color = ColorGetter.BG
        canvas.drawRect(fullRect, paint)


        paint.color = RIPPLE_COLOR
        paint.alpha = (rippleAnimator.bgPercentage * 255).toInt()
        canvas.drawRect(fullRect, paint)

        rippleAnimator.drawCircles { pos, r, alpha, _ ->
            paint.alpha = (255 * alpha).toInt()
            canvas.drawCircle(pos.x, pos.y, r, paint)
        }
        super.draw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        rippleAnimator.onTouchEvent(event)
        super.onTouchEvent(event)
        return true
    }


}