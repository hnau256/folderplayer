package ru.hnau.folderplayer.utils.managers.ux.dialog_manager

import android.content.Context
import ru.hnau.folderplayer.activity.view.button.TextButton
import ru.hnau.folderplayer.utils.getters.StringGetter


class DialogButton(
        context: Context,
        text: StringGetter,
        onClickListener: () -> Unit,
        private val closeDialogOnCLick: Boolean = true,
        size: Size = Size.MIDDLE,
        color: Color = Color.NORMAL
) : TextButton(
        context,
        text,
        {
            onClickListener.invoke()
            if (closeDialogOnCLick) {
                DialogManager.close()
            }
        },
        size,
        color
)