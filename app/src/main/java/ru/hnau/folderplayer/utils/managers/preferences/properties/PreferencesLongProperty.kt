package ru.hnau.folderplayer.utils.managers.preferences.properties

import android.content.Context
import android.content.SharedPreferences

/**
 * Свойство типа Long
 */
class PreferencesLongProperty(key: String, defaultValue: Long = 0L) : PreferencesProperty<Long>(key, defaultValue) {

    override fun readValue(container: SharedPreferences, key: String) = container.getLong(key, defaultValue)

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: Long) {
        editor.putLong(key, value)
    }
}