package ru.hnau.folderplayer.utils.ui.drawable

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable

/**
 * Drawable с кешированием изображдения для отрисовки при сохраняющемся размере вывода
 */
abstract class TranslateBeforeDrawDrawable : Drawable() {

    var drawAlpha: Int = 255

    override fun draw(canvas: Canvas) {
        canvas.save()
        canvas.translate(bounds.left.toFloat(), bounds.top.toFloat())
        draw(canvas, bounds.width(), bounds.height())
        canvas.restore()
    }

    protected abstract fun draw(canvas: Canvas, width: Int, height: Int)

    override fun setAlpha(alpha: Int) {
        drawAlpha = alpha
    }

    override fun getOpacity() = PixelFormat.TRANSLUCENT

    override fun setColorFilter(colorFilter: ColorFilter?) {}
}