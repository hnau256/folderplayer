package ru.hnau.folderplayer.utils.ui.drawable

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable


class EmptyDrawable: Drawable() {

    override fun draw(canvas: Canvas?) {}

    override fun setAlpha(alpha: Int) {}

    override fun getOpacity() = PixelFormat.TRANSPARENT

    override fun setColorFilter(colorFilter: ColorFilter?) {}

}