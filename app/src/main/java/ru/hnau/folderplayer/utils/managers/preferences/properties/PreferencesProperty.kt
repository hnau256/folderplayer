package ru.hnau.folderplayer.utils.managers.preferences.properties

import android.content.Context
import android.content.SharedPreferences
import ru.hnau.folderplayer.utils.managers.ContextContainer
import ru.hnau.folderplayer.utils.managers.Logger
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Свойство любого типа
 */
abstract class PreferencesProperty<T>(private val key: String, val defaultValue: T) : ReadWriteProperty<Any, T> {
    private val TAG = PreferencesProperty::class.java.simpleName

    private var value: T? = null

    private val preferences: SharedPreferences?
        get() = ContextContainer.context.getSharedPreferences("GENERAL_PREFERENCES", 0)

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        var result = value
        if (value == null) {
            result = preferences?.let { preferences -> readValue(preferences, key) }
            value = result
            Logger.d(TAG, "Reading property $key: $result")
        }

        return result ?: defaultValue
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        synchronized(this, {
            if (!valueIsMutable() && value == this.value) {
                Logger.d(TAG, "No need to save value $value to property $key, it already saved")
                return
            }
            this.value = value
            val editor = preferences?.edit()
            editor?.let { writeValue(editor, key, value) }
            editor?.apply()
            Logger.d(TAG, "Saving property $key with value $value")
        })
    }

    abstract fun readValue(container: SharedPreferences, key: String): T

    abstract fun writeValue(editor: SharedPreferences.Editor, key: String, value: T)

    protected open fun valueIsMutable() = false

}