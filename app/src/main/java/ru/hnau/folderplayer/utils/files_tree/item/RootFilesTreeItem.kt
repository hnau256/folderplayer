package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import android.os.Environment
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.audio.PlayItem
import ru.hnau.folderplayer.utils.ui.drawable.EmptyDrawable
import java.io.File


class RootFilesTreeItem(
        context: Context
) : FilesTreeItem(
        context,
        DrawableGetter(EmptyDrawable())
), OpenableFilesTreeItem {

    override fun getSubitems(): List<FilesTreeItem> =
            arrayListOf<FilesTreeItem>(
                    FsRootFilesTreeItem(context),
                    BookmarksFilesTreeItem(context)
            ) + getStorages(context)

    override fun getParent() = null

    override fun getTitle() = StringGetter(R.string.fs_item_title_root)

    override fun getPlayItemRelation(playItem: PlayItem) = PlayItemRelationChecker.Type.PARENT


    companion object {

        fun getStorages(context: Context): List<FilesTreeItem> {

            val result = ArrayList<String>()

            if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                val externalStorage = Environment.getExternalStorageDirectory()?.absolutePath?.trim()
                if (externalStorage != null && externalStorage.isNotEmpty()) {
                    result.add(externalStorage)
                }
            }

            val removableStorages =
                    (System.getenv("SECONDARY_STORAGE")
                            ?.trim()
                            ?.split(File.pathSeparator) ?: emptyList())
                            .mapNotNull {
                                it.trim().let {
                                    if (it.isEmpty()) null else it
                                }
                            }
            result.addAll(removableStorages)

            val storageFile = File("/storage")
            if (storageFile.isDirectory) {
                val storages = storageFile.listFiles()?.mapNotNull {
                    val storageSubitems = it.listFiles()
                    if (storageSubitems == null || storageSubitems.isEmpty()) {
                        null
                    } else {
                        it.absolutePath
                    }
                }
                storages?.let { result.addAll(storages) }
            }

            return result.distinct().mapIndexed { i, path ->
                val title = context.getString(R.string.fs_item_title_storage) + " ${i + 1}"
                StorageFilesTreeItem(context, path, StringGetter(title))
            }
        }

    }
}