package ru.hnau.folderplayer.activity.view.label

import android.text.TextUtils
import android.view.Gravity
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter


data class TextViewInfo(
        val size: SizeGetter = SizeGetter(16),
        val color: Int = ColorGetter.FG,
        val gravity: Int = Gravity.CENTER_VERTICAL or Gravity.LEFT,
        val maxLines: Int? = null,
        val minLines: Int? = null,
        val ellipsize: TextUtils.TruncateAt = TextUtils.TruncateAt.END
)