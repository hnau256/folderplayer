package ru.hnau.folderplayer.activity.screen_pages.equalizer

import android.graphics.Path
import android.graphics.PointF


class BezierPath(
        basePoints: List<PointF>,
        pixelsForSegment: Int = DEFAULT_PIXELS_FOR_SEGMENT,
        tension: Float = DEFAULT_TENSION
): Path() {

    companion object {
        private val DEFAULT_PIXELS_FOR_SEGMENT = 1
        private val DEFAULT_TENSION = 0.5f
    }

    init {
        init(basePoints = basePoints, tension = tension, pixelsForSegment = pixelsForSegment)
    }


    fun init(basePoints: List<PointF>, tension: Float, pixelsForSegment: Int) {
        if (basePoints.size < 2) {
            return
        }

        moveTo(basePoints[0].x, basePoints[0].y)

        val pointsCount = basePoints.size + 2

        for (i in 1 until pointsCount - 2) {
            val p2 = getPoint(basePoints, i)
            val p3 = getPoint(basePoints, i + 1)

            val segmentsCount = Math.round((Math.abs(p2.x - p3.x) / pixelsForSegment))
            if (segmentsCount <= 1) {
                lineTo(p3)
                continue
            }

            val p1 = getPoint(basePoints, i - 1)
            val p4 = getPoint(basePoints, i + 2)

            val t1x = (p3.x - p1.x) * tension
            val t2x = (p4.x - p2.x) * tension
            val t1y = (p3.y - p1.y) * tension
            val t2y = (p4.y - p2.y) * tension

            for (t in 0..segmentsCount) {
                val st = t.toFloat() / segmentsCount
                val st2 = st * st
                val st3 = st2 * st

                val c1 = 2 * st3 - 3 * st2 + 1
                val c2 = -2 * st3 + 3 * st2
                val c3 = st3 - 2 * st2 + st
                val c4 = st3 - st2

                val x = c1 * p2.x + c2 * p3.x + c3 * t1x + c4 * t2x
                val y = c1 * p2.y + c2 * p3.y + c3 * t1y + c4 * t2y

                lineTo(x, y)
            }
        }

        lineTo(basePoints[basePoints.size - 1])

    }

    private fun lineTo(point: PointF) {
        lineTo(point.x, point.y)
    }

    private fun getPoint(basePoints: List<PointF>, i: Int): PointF {
        var i = i
        val size = basePoints.size
        i = if (i == 1 || i == 0) 0 else if (i == size + 1) size - 1 else i - 1
        return basePoints[i]
    }

}