package ru.hnau.folderplayer.activity.view.list

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


open class BaseList<T>(
        context: Context,
        layoutType: LayoutType = LayoutType.VERTICAL
) : RecyclerView(context) {

    enum class LayoutType(val layoutManagerBuilder: (context: Context) -> RecyclerView.LayoutManager) {
        HORIZONTAL({ LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false) }),
        VERTICAL({ LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false) }),
        HORIZONTAL_REVERSE({ LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, true) }),
        VERTICAL_REVERSE({ LinearLayoutManager(it, LinearLayoutManager.VERTICAL, true) })
    }

    var baseListAdapter: BaseListAdapter<T>?
        set(value) {
            this.adapter = value
        }
        get() = this.adapter as? BaseListAdapter<T>

    var fistVisibleItemPosition: Int
        set(value) {
            (layoutManager as? LinearLayoutManager)?.scrollToPosition(value)
        }
        get() = (layoutManager as? LinearLayoutManager)?.findFirstVisibleItemPosition() ?: 0

    init {
        this.layoutManager = layoutType.layoutManagerBuilder.invoke(context)
    }


}