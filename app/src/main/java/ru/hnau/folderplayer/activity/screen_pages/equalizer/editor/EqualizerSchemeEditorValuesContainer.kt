package ru.hnau.folderplayer.activity.screen_pages.equalizer.editor

import android.support.v4.view.animation.FastOutSlowInInterpolator
import ru.hnau.folderplayer.utils.DataScroller
import ru.hnau.folderplayer.utils.setSize


class EqualizerSchemeEditorValuesContainer(
        private val bandsCount: Int,
        private val onValuesChanged: (List<Float>) -> Unit
) {

    private val dataScroller = DataScroller<MutableList<Float>>(
            iterator = this::onDataScrolled,
            scrollTime = 200,
            scrollInterpolator = FastOutSlowInInterpolator()
    )

    var data: MutableList<Float> = (0 until bandsCount).map { 0f }.toMutableList()
        private set(value) {
            field = value
            onValuesChanged.invoke(value)
        }

    fun setValues(values: List<Float>, animate: Boolean = true) {
        val normalizedValues = values.toMutableList().apply { setSize(bandsCount, 0f) }
        dataScroller.setNewData(normalizedValues, animate)
    }

    fun setValue(band: Int, value: Float) {
        if (dataScroller.animating || band < 0 || band >= bandsCount) {
            return
        }
        this.data = dataScroller.currentData ?: return
        this.data[band] = value
        onValuesChanged.invoke(this.data)
    }

    private fun onDataScrolled(from: MutableList<Float>?, to: MutableList<Float>?, percentage: Float) {
        this.data = getInterpolation(from, to, percentage) ?: return
    }

    private fun getInterpolation(from: MutableList<Float>?, to: MutableList<Float>?, percentage: Float): MutableList<Float>? {
        if (to == null) {
            return null
        }
        if (from == null) {
            return to
        }
        return from.mapIndexed { index, valueFrom ->
            val valueTo = to[index]
            valueFrom + (valueTo - valueFrom) * percentage
        }.toMutableList()
    }


}