package ru.hnau.folderplayer.activity.screen_pages.files_explorer

import android.content.Context
import ru.hnau.folderplayer.activity.screen_pages.files_explorer.files_explorer_list.FilesTreeList
import ru.hnau.folderplayer.activity.view.views_switcher.ViewsSwitcher
import ru.hnau.folderplayer.utils.files_tree.FileTreeItemStorageStringConverter
import ru.hnau.folderplayer.utils.files_tree.item.FilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.FsFileFilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.OpenableFilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.RootFilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.toStorageString
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManagerStarter
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager
import java.util.*


class FilesListsStackView(context: Context) : ViewsSwitcher(context) {

    private val positionsStack = Stack<Int>()

    private var currentItem = getInitialItem()
        set(value) {
            if (field != value) {
                field = value
                onCurrentItemChanged(value)
            }
        }

    private var currentItemParent: OpenableFilesTreeItem? = null

    private var currentList: FilesTreeList? = null
        set(value) {
            if (field != value) {
                field = value
                if (value != null) {
                    showView(value, if (goingBack) Side.LEFT else Side.RIGHT)
                    addView(value)
                }
            }
        }

    private var goingBack = false

    init {
        onCurrentItemChanged(currentItem)
    }

    private fun getInitialItem() =
            FileTreeItemStorageStringConverter.getFilesTreeItem(context, PreferencesManager.filesExplorerViewPath) as? OpenableFilesTreeItem ?: RootFilesTreeItem(context)

    private fun onItemClicked(item: FilesTreeItem) {
        if (item is FsFileFilesTreeItem && item.audio) {
            saveCurrentItemToStorage()
            PlayerSessionManagerStarter.onAudioClicked(item)
            return
        }
        if (item is OpenableFilesTreeItem) {
            openOpenableItem(item, false)
        }
    }

    fun openOpenableItem(item: OpenableFilesTreeItem) = openOpenableItem(item, true)

    private fun openOpenableItem(item: OpenableFilesTreeItem, clearStack: Boolean) {
        if (FilesTreeItem.isEquals(currentItem as? FilesTreeItem, item as? FilesTreeItem)) {
            return
        }
        val fistVisibleItemPosition = currentList?.fistVisibleItemPosition ?: 0
        positionsStack.push(fistVisibleItemPosition)
        currentItem = item
        if (clearStack) {
            positionsStack.clear()
        }
    }

    fun goHome() {
        if (currentItem is RootFilesTreeItem) {
            return
        }
        goingBack = true
        openOpenableItem(RootFilesTreeItem(context))
        goingBack = false
        positionsStack.clear()

    }

    private fun onCurrentItemChanged(currentItem: OpenableFilesTreeItem) {
        currentItemParent = (currentItem as? FilesTreeItem)?.getParent()
        currentList = FilesTreeList(
                context,
                currentItem,
                this::onItemClicked
        )
    }

    fun handleGoBack(): Boolean {
        val currentItemParent = this.currentItemParent ?: return false
        goingBack = true
        currentItem = currentItemParent
        val positionToScroll = if (positionsStack.empty()) 0 else positionsStack.pop() ?: 0
        currentList?.fistVisibleItemPosition = positionToScroll
        goingBack = false
        return true
    }

    private fun saveCurrentItemToStorage() {
        PreferencesManager.filesExplorerViewPath = (currentItem as? FilesTreeItem)?.toStorageString() ?: ""
    }

}