package ru.hnau.folderplayer.activity.screen_pages.files_explorer.player_control_panel

import android.content.Context
import android.view.Gravity
import android.widget.LinearLayout
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.view.button.DarkIconButton
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.audio.PlayerManager
import ru.hnau.folderplayer.utils.managers.audio.RepeatManager
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManager
import ru.hnau.folderplayer.utils.ui.ScreenManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.AnimatorsMetronome
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class PlayerControlPanelInnerView(context: Context) : LinearLayout(context) {

    private val BUTTON_HEIGHT = ScreenManager.sizePx.x / 5

    private val onAnimationTic = {
        state = PlayerManager.state
        onPercentageChanged()
    }

    private var state = PlayerManager.state
        set(value) {
            if (field != value) {
                field = value
                onStateChanged()
            }
        }


    private val progressView = PlayerControlPanelProgressView(context, {}, {
        PlayerManager.percentage = it
        val position = Math.round(it * 100).toString() + "%"
        FirebaseManager.sendEvent("Play position selected", mapOf("position" to position))
        onPercentageChanged()
    }).apply {
        setBackgroundColor(ColorGetter.BG)
        val paddingH = dpToPxInt(20)
        val paddingTop = dpToPxInt(20)
        val paddingBottom = dpToPxInt(12)
        setPadding(paddingH, paddingTop, paddingH, paddingBottom)
    }

    private val currentTimeView = PlayerControlPanelTimeView(context, Gravity.LEFT).apply {
        layoutParams = LinearLayout.LayoutParams(0, UiUtils.WRAP_CONTENT, 1f)
    }

    private val totalTimeView = PlayerControlPanelTimeView(context, Gravity.RIGHT).apply {
        layoutParams = LinearLayout.LayoutParams(0, UiUtils.WRAP_CONTENT, 1f)
    }

    private val labelsContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER
        setBackgroundColor(ColorGetter.BG)
        val paddingH = dpToPxInt(20)
        val paddingTop = dpToPxInt(0)
        val paddingBottom = dpToPxInt(4)
        setPadding(paddingH, paddingTop, paddingH, paddingBottom)
        addView(currentTimeView)
        addView(totalTimeView)
    }

    private val repeatTypeButton = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(RepeatManager.type.iconResId),
            onClick = {
                RepeatManager.switchToNextType()
                onRepeatTypeChanged()
                FirebaseManager.sendEvent("Repeat type button clicked")
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val prevTypeButton = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_media_prev),
            onClick = {
                PlayerSessionManager.prev()
                FirebaseManager.sendEvent("Previous file button clicked")
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val playPauseTypeButton = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_media_pause),
            onClick = {
                PlayerSessionManager.pauseOrResume()
                FirebaseManager.sendEvent("Resume or pause button clicked")
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val nextTypeButton = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_media_next),
            onClick = {
                PlayerSessionManager.next()
                FirebaseManager.sendEvent("Next file button clicked")
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val stopTypeButton = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_media_stop),
            onClick = {
                PlayerSessionManager.stop()
                FirebaseManager.sendEvent("Stop button clicked")
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val buttonsContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER

        addView(repeatTypeButton)
        addView(prevTypeButton)
        addView(playPauseTypeButton)
        addView(nextTypeButton)
        addView(stopTypeButton)

    }

    init {
        orientation = LinearLayout.VERTICAL
        gravity = Gravity.CENTER
        setBackgroundColor(ColorGetter.BG_DARK)

        addView(progressView)
        addView(labelsContainer)
        addView(buttonsContainer)

        onStateChanged()
        onPercentageChanged()
    }

    private fun timeContainsHours(time: Int) = time >= 60 * 60 * 1000

    private fun onStateChanged() {
        if (PlayerManager.state.currentItem == null) {
            return
        }
        playPauseTypeButton.icon = if (state.paused) DrawableGetter(R.drawable.ic_media_play) else DrawableGetter(R.drawable.ic_media_pause)
        requestLayout()
    }

    private fun onRepeatTypeChanged() {
        repeatTypeButton.icon = DrawableGetter(RepeatManager.type.iconResId)
    }

    private fun onPercentageChanged() {
        if (PlayerManager.state.currentItem == null) {
            return
        }

        progressView.progress = PlayerManager.percentage

        val duration = PlayerManager.duration
        val showHours = timeContainsHours(duration)
        currentTimeView.showHours = showHours
        currentTimeView.time = PlayerManager.position
        totalTimeView.showHours = showHours
        totalTimeView.time = duration
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        AnimatorsMetronome.addListener(onAnimationTic)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        AnimatorsMetronome.removeListener(onAnimationTic)
    }

}