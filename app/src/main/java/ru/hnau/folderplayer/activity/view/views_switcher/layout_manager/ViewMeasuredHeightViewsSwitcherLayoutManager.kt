package ru.hnau.folderplayer.activity.view.views_switcher.layout_manager

import android.graphics.Rect
import android.view.Gravity
import android.view.View

/**
 * Определение высоты прокручиваемого элемента внутри ViewsSwitcher на основе View.measuredHeight
 */
class ViewMeasuredHeightViewsSwitcherLayoutManager(val gravity: Int = Gravity.CENTER) : ViewsSwitcherLayoutManager {

    override fun layoutView(view: View, width: Int, height: Int): Rect {
        view.measure(View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        return Rect(0, 0, width, view.measuredHeight)
    }

}