package ru.hnau.folderplayer.activity.screen_pages.settings.item.view

import android.content.Context
import android.text.TextUtils
import android.view.Gravity
import ru.hnau.folderplayer.activity.screen_pages.settings.item.SettingsItem
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter


class SettingsItemTitleView(context: Context) : Label(
        context = context,
        size = SizeGetter(20),
        color = ColorGetter.FG,
        gravity = Gravity.CENTER_VERTICAL or Gravity.LEFT,
        maxLines = 1,
        minLines = 1,
        ellipsize = TextUtils.TruncateAt.END
) {

    init {
        val paddingH = SettingsItem.PADDING_H
        val paddingV = SettingsItem.PADDING_V
        setPadding(paddingH, paddingV, paddingH, paddingV / 2)
    }

}