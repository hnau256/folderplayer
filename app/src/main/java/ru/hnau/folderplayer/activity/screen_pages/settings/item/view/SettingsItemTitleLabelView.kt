package ru.hnau.folderplayer.activity.screen_pages.settings.item.view

import android.content.Context
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.managers.ContextContainer.context


class SettingsItemTitleLabelView(
        context: Context
): Label(
        context = context,
        size = SizeGetter(17),
        color = ColorGetter.FG_TRANSPARENT_50,
        maxLines = 3,
        minLines = 1
)