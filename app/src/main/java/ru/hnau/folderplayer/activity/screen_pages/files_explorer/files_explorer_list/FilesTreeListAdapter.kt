package ru.hnau.folderplayer.activity.screen_pages.files_explorer.files_explorer_list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import ru.hnau.folderplayer.activity.view.list.BaseListAdapter
import ru.hnau.folderplayer.utils.files_tree.item.FilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.OpenableFilesTreeItem
import ru.hnau.folderplayer.utils.findPos
import ru.hnau.folderplayer.utils.ui.UiUtils


class FilesTreeListAdapter(
        val list: FilesTreeList,
        private val baseItem: OpenableFilesTreeItem,
        private val onItemClickCallback: (FilesTreeItem) -> Unit
) : BaseListAdapter<FilesTreeItem>() {

    val context = list.context!!

    private var subitems = getSubitems()

    override fun getItem(position: Int) = subitems[position]

    override fun bindItem(view: View, item: FilesTreeItem, type: Int) {
        (view as? FilesTreeListItemView)?.item = item
    }

    override fun generateView(type: Int) = FilesTreeListItemView(
            context = context,
            onItemClickCallback = onItemClickCallback,
            onItemRemovedCallback = this::onItemRemoved
    ).apply {
        layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
            bottomMargin = UiUtils.ELEMENTS_SEPARATION
        }
    }

    private fun onItemRemoved(item: FilesTreeItem) {
        val position = subitems.findPos { it == item }
        subitems = getSubitems()
        if (position == null) {
            notifyDataSetChanged()
            return
        }
        notifyItemRemoved(position)
    }

    private fun getSubitems() = baseItem.getSubitems().sorted()

    override fun getItemCount() = subitems.size
}