package ru.hnau.folderplayer.activity.view.bottom_animator

import android.view.View
import android.view.ViewGroup
import ru.hnau.folderplayer.activity.view.views_switcher.ViewsSwitcher
import ru.hnau.folderplayer.activity.view.views_switcher.layout_manager.ViewMeasuredHeightViewsSwitcherLayoutManager
import ru.hnau.folderplayer.utils.ui.ScreenManager
import ru.hnau.folderplayer.utils.ui.UiUtils


open class BottomAnimator(private val fgView: View) : ViewGroup(fgView.context) {

    private val MAX_BOTTOM_VIEW_HEIGHT = ScreenManager.sizePx.y / 2

    private val viewsSwitcher = ViewsSwitcher(context).apply {
        layoutManager = ViewMeasuredHeightViewsSwitcherLayoutManager()
    }

    private var currentGoBackHandler: (() -> Boolean)? = null
    private var currentHideCallback: (() -> Unit)? = null

    private val shadowView = BottomAnimatorShadowView(
            context = context,
            onClickListener = { handleGoBack() }
    )

    init {
        addView(fgView)
        addView(shadowView)
        addView(viewsSwitcher)
    }

    fun showBottomView(view: BottomAnimatorView) {
        currentHideCallback?.invoke()
        shadowView.active = true
        currentGoBackHandler = view.goBackHandler
        currentHideCallback = view.onHideCallback
        val first = viewsSwitcher.currentView == null
        viewsSwitcher.showView(view.view, if (first) ViewsSwitcher.Side.BOTTOM else ViewsSwitcher.Side.RIGHT)
    }

    fun hideBottomView() {
        currentHideCallback?.invoke()
        shadowView.active = false
        currentGoBackHandler = null
        currentHideCallback = null
        viewsSwitcher.showView(null, ViewsSwitcher.Side.TOP)
    }

    fun handleGoBack(): Boolean {
        if (currentGoBackHandler?.invoke() == true) {
            return true
        }
        if (viewsSwitcher.currentView != null) {
            hideBottomView()
            return true
        }
        return false
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val width = this.width
        val height = this.height
        val offset = viewsSwitcher.measuredHeight

        fgView.layout(0, 0, width, height)
        shadowView.layout(0, 0, width, height)
        viewsSwitcher.layout(0, height - offset, width, height)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = UiUtils.getMaxSize(0, widthMeasureSpec)
        val height = UiUtils.getMaxSize(0, heightMeasureSpec)
        viewsSwitcher.measure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(MAX_BOTTOM_VIEW_HEIGHT, MeasureSpec.AT_MOST)
        )

        shadowView.measure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        )

        fgView.measure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        )

        setMeasuredDimension(width, height)
    }


}