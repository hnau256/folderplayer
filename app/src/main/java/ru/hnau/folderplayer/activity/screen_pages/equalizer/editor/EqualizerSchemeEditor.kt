package ru.hnau.folderplayer.activity.screen_pages.equalizer.editor

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.Rect
import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.view.MotionEvent
import android.view.View
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.screen_pages.equalizer.BezierPath
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager
import ru.hnau.folderplayer.utils.ui.FontManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.TwoStateAnimator
import ru.hnau.folderplayer.utils.ui.dpToPx
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class EqualizerSchemeEditor(context: Context) : View(context) {


    companion object {

        private val HZ_TITLE = StringGetter(R.string.equalizer_screen_page_freq_hz)
        private val KHZ_TITLE = StringGetter(R.string.equalizer_screen_page_freq_khz)


        private val TITLE_COLOR = ColorGetter.FG_TRANSPARENT_50
        private val BAND_COLOR = ColorGetter.BG_DARK
        private val CIRCLE_COLOR = ColorGetter.FG
        private val LINE_COLOR = ColorGetter.FG

    }

    private val TITLE_TEXT_SIZE = dpToPx(15)
    private val TITLE_OFFSET_Y = TITLE_TEXT_SIZE + dpToPx(8)

    private val LINE_WIDTH = dpToPx(3)

    private val BAND_WIDTH = dpToPxInt(8)
    private val BAND_WIDTH_DIV_2 = BAND_WIDTH / 2
    private val CIRCLE_R = dpToPxInt(8)

    private val PADDING_H = dpToPxInt(48)
    private val PADDING_TOP = dpToPxInt(32)
    private val PADDING_BOTTOM = dpToPxInt(48)

    private var schemeId: Long = -1
        set(value) {
            if (field != value) {
                val oldField = field
                field = value
                onNewSchemeSelected(oldField, value)
            }
        }

    private val schemeEditableAnimator = TwoStateAnimator().apply {
        time = 200L
        interpolatorPositive = LinearOutSlowInInterpolator()
        interpolatorNegative = FastOutLinearInInterpolator()
        iterator = { invalidate() }
    }

    private val bandsCount = EqualizerManager.bandsCount.toInt()

    private val values = EqualizerSchemeEditorValuesContainer(
            bandsCount = bandsCount,
            onValuesChanged = { invalidate() }
    )

    private var editingBand: Int? = null

    private var editingValueDelta = 0f

    private val data: List<Float>
        get() = values.data

    private val onSchemeValueChangedListener = { band: Int ->
        values.setValue(band, EqualizerManager.getSchemeValue(schemeId, band))
    }

    private val onSchemeSelectedListener = { schemeId: Long -> this.schemeId = schemeId }

    private var initialized = false

    private var attachedToWindow = false

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val linePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = LINE_COLOR
        style = Paint.Style.STROKE
        strokeWidth = LINE_WIDTH
        strokeCap = Paint.Cap.ROUND
        strokeJoin = Paint.Join.ROUND
    }

    private val titlePaint = FontManager.getPaint().apply {
        color = TITLE_COLOR
        textAlign = Paint.Align.CENTER
        textSize = TITLE_TEXT_SIZE
    }

    init {
        setBackgroundColor(ColorGetter.BG)
        schemeId = EqualizerManager.currentSchemeId
        initialized = true
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        val left = PADDING_H
        val right = width - PADDING_H
        val top = PADDING_TOP
        val bottom = height - PADDING_BOTTOM

        val bandsCount = this.bandsCount
        if (bandsCount < 2) {
            return
        }
        val bandsD = (right - left).toFloat() / (bandsCount - 1).toFloat()

        val bandsXs = (0 until bandsCount).map { left + (bandsD * it) }

        drawBands(canvas, top, bottom, bandsXs)

        val linePoints = (0 until bandsCount).map { band ->
            val x = left + (bandsD * band)
            val bandValue = data.getOrNull(band) ?: 0f
            val value = 1 - (bandValue + 1f) / 2
            val y = top + (bottom - top) * value
            PointF(x, y)
        }

        drawLine(canvas, linePoints)
        drawCircles(canvas, linePoints)
        drawTitles(canvas, bandsXs, bottom)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val value = yToNotFormattedValue(event.y) ?: return false
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val band = xToBand(event.x) ?: return false
                if (!EqualizerManager.isSchemeEditable(EqualizerManager.currentSchemeId)) {
                    return false
                }
                editingBand = band
                editingValueDelta = value - EqualizerManager.getSchemeValue(EqualizerManager.currentSchemeId, band)
            }
            MotionEvent.ACTION_MOVE -> {
                val editingBand = editingBand ?: return false
                val rawValue = value - editingValueDelta
                val formattedValue = if (rawValue < -1) -1f else if (rawValue > 1f) 1f else rawValue
                EqualizerManager.setSchemeValue(EqualizerManager.currentSchemeId, editingBand, formattedValue)
            }
            else -> {
                editingBand = null
            }
        }
        return true
    }

    private fun xToBand(x: Float): Int? {
        val left = PADDING_H
        val right = width - PADDING_H

        val bandsCount = this.bandsCount
        if (bandsCount < 2) {
            return null
        }
        val bandsD = (right - left).toFloat() / (bandsCount - 1).toFloat()
        if (bandsD <= 0) {
            return null
        }

        val bandRaw = (x - left) / bandsD
        return Math.round(bandRaw).let { if (it < 0) 0 else if (it >= bandsCount) bandsCount - 1 else it }
    }

    private fun yToNotFormattedValue(y: Float): Float? {
        val top = PADDING_TOP
        val bottom = height - PADDING_BOTTOM
        if (top == bottom) {
            return null
        }

        val rawValue = (y - top.toFloat()) / (bottom - top).toFloat()

        return 1f - rawValue * 2f
    }

    private fun drawBands(canvas: Canvas, top: Int, bottom: Int, bandsXs: List<Float>) {
        paint.color = BAND_COLOR

        bandsXs.forEach {
            val x = it.toInt()
            val bandRect = Rect(x - BAND_WIDTH_DIV_2, top, x + BAND_WIDTH_DIV_2, bottom)
            val bandPath = UiUtils.calcRoundSidesPath(bandRect)
            canvas.drawPath(bandPath, paint)
        }
    }

    private fun drawCircles(canvas: Canvas, linePoints: List<PointF>) {
        val circleRadius = schemeEditableAnimator.currentValue * CIRCLE_R
        paint.color = CIRCLE_COLOR
        if (circleRadius <= 0) {
            return
        }

        linePoints.forEach {
            canvas.drawCircle(it.x, it.y, circleRadius, paint)
        }
    }

    private fun drawLine(canvas: Canvas, linePoints: List<PointF>) {
        val linePath = BezierPath(linePoints)
        canvas.drawPath(linePath, linePaint)
    }

    private fun drawTitles(canvas: Canvas, bandsXs: List<Float>, bottom: Int) {
        val y = bottom + TITLE_OFFSET_Y
        bandsXs.forEachIndexed { band, x ->
            val freq = EqualizerManager.getBandFreq(band)
            val title = freqToText(freq)
            canvas.drawText(title, x, y, titlePaint)
        }
    }

    private fun freqToText(freq: Int): String {
        val khz = freq >= 1000

        val suffix = (if (khz) KHZ_TITLE else HZ_TITLE).get(context)

        val normalizedFreq = if (khz)
            if (freq % 1000 == 0)
                (freq / 1000).toString()
            else
                (Math.round(freq / 100f) / 10f).toString()
        else
            freq.toString()

        return "$normalizedFreq $suffix"
    }

    private fun onNewSchemeSelected(oldId: Long, id: Long) {
        EqualizerManager.removeOnSchemeBandChangedListener(oldId, onSchemeValueChangedListener)
        if (attachedToWindow) {
            EqualizerManager.addOnSchemeBandChangedListener(id, onSchemeValueChangedListener)
        }

        val editable = EqualizerManager.isSchemeEditable(id)
        schemeEditableAnimator.addNewTarget(editable, !initialized)
        val values = (0 until bandsCount).map { EqualizerManager.getSchemeValue(id, it) }
        this.values.setValues(values, initialized)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        attachedToWindow = true
        EqualizerManager.addOnSelectedSchemeSelectedListener(onSchemeSelectedListener)
        EqualizerManager.addOnSchemeBandChangedListener(schemeId, onSchemeValueChangedListener)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        attachedToWindow = false
        EqualizerManager.removeOnSelectedSchemeSelectedListener(onSchemeSelectedListener)
        EqualizerManager.removeOnSchemeBandChangedListener(schemeId, onSchemeValueChangedListener)
    }


}