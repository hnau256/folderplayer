package ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise


interface SettingsItemChoiceViewItem {

    fun getDescription(): String

}