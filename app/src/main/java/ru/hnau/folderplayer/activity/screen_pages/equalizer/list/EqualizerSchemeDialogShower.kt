package ru.hnau.folderplayer.activity.screen_pages.equalizer.list

import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.view.text_input.TextInput
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.AppActivityManager
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager
import ru.hnau.folderplayer.utils.managers.ux.KeyboardManager
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.DialogManager
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.actions.DialogAction


object EqualizerSchemeDialogShower {

    private enum class Actions(val forNotEditable: Boolean, val titleResId: Int, val action: (id: Long) -> Unit, val closeOnClick: Boolean) {
        RENAME(
                forNotEditable = false,
                titleResId = R.string.equalizer_screen_page_item_actions_rename,
                action = EqualizerSchemeDialogShower::showRenameDialog,
                closeOnClick = false
        ),
        DUPLICATE(
                forNotEditable = true,
                titleResId = R.string.equalizer_screen_page_item_actions_duplicate,
                action = EqualizerManager::duplicateScheme,
                closeOnClick = true
        ),
        REMOVE(
                forNotEditable = false,
                titleResId = R.string.equalizer_screen_page_item_actions_remove,
                action = EqualizerManager::removeScheme,
                closeOnClick = true
        );

        companion object {

            val ACTIONS_FOR_EDITABLE = values().toList()
            val ACTIONS_FOR_NOT_EDITABLE = values().filter { it.forNotEditable }

        }

    }

    fun show(id: Long) {
        val name = StringGetter(EqualizerManager.getSchemeName(id))
        val editable = EqualizerManager.isSchemeEditable(id)
        val actions = if (editable) Actions.ACTIONS_FOR_EDITABLE else Actions.ACTIONS_FOR_NOT_EDITABLE

        DialogManager.show {
            titleForActions(name)
            content(actions.map { action ->
                DialogAction(
                        title = StringGetter(action.titleResId),
                        action = { action.action.invoke(id) },
                        closeOnClick = action.closeOnClick
                )
            })
        }
    }

    private fun showRenameDialog(id: Long) {
        val context = AppActivityManager.activity ?: return
        val name = EqualizerManager.getSchemeName(id)

        val textInput = TextInput(
                context = context,
                initialTitle = StringGetter(R.string.equalizer_screen_page_item_actions_rename_dialog_text_input_title),
                initialText = name
        ).apply {
            imeOptions = EditorInfo.IME_ACTION_DONE
        }

        val view = FrameLayout(context).apply {
            setBackgroundColor(ColorGetter.BG)
            setPadding(DialogManager.PADDING_H, 0, DialogManager.PADDING_H, DialogManager.PADDING_V)
            addView(textInput)
        }

        DialogManager.show {
            title(StringGetter(name))
            content(view)
            addButton(
                    text = StringGetter(R.string.equalizer_screen_page_item_actions_rename_dialog_button_cancel),
                    onClickListener = {}
            )
            addButton(
                    text = StringGetter(R.string.equalizer_screen_page_item_actions_rename_dialog_button_save),
                    onClickListener = { EqualizerManager.setSchemeName(id, textInput.text.toString()) }
            )
            onCancelledCallback { KeyboardManager.hide() }
        }

        KeyboardManager.showAndRequestFocus(textInput)
    }

}