package ru.hnau.folderplayer.activity.view.bottom_animator

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.view.MotionEvent
import android.view.View
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.TwoStateAnimator


class BottomAnimatorShadowView(
        context: Context,
        private val onClickListener: () -> Unit
) : View(context) {

    private val COLOR = UiUtils.setColorAlpha(Color.BLACK, 0.75f)

    private val activeAnimator = TwoStateAnimator().apply {
        time = 200
        iterator = { invalidate() }
    }

    var active = false
        set(value) {
            if (field != value) {
                field = value
                activeAnimator.addNewTarget(active, false)
            }
        }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (active) {
            onTouchWhenActive(event)
            return true
        }
        return super.onTouchEvent(event)
    }

    private fun onTouchWhenActive(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_UP && event.x >= 0 && event.x < width && event.y >= 0 && event.y < height) {
            onClickListener.invoke()
        }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        canvas.drawColor(UiUtils.setColorAlpha(COLOR, activeAnimator.currentValue))
    }

}