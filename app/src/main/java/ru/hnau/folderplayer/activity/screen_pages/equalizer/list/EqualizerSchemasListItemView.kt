package ru.hnau.folderplayer.activity.screen_pages.equalizer.list

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.text.TextUtils
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.view.ClickableLinearLayout
import ru.hnau.folderplayer.activity.view.button.DarkIconButton
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.files_tree.item.PlayItemRelationChecker
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.ripple_animator.RippleAnimator
import ru.hnau.folderplayer.utils.ui.dpToPxInt
import ru.hnau.folderplayer.utils.ui.drawable.layout_drawable.LayoutDrawableView


class EqualizerSchemasListItemView(context: Context) : ClickableLinearLayout(context) {

    var id: Long = 0L
        set(value) {
            val oldField = field
            field = value
            bindScheme(oldField, value)
        }

    private var selectedState = false
        set(value) {
            if (field != value) {
                field = value
                onSelectedStateChanged()
            }
        }

    private val iconView = EqualizerSchemasListItemIconView(context).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.WRAP_CONTENT, UiUtils.WRAP_CONTENT).apply {
            val marginH = dpToPxInt(16)
            leftMargin = marginH
            rightMargin = marginH
        }
    }

    private val titleView = Label(
            context = context,
            gravity = Gravity.LEFT or Gravity.CENTER_VERTICAL,
            size = SizeGetter(20),
            color = ColorGetter.FG,
            maxLines = 1,
            minLines = 1,
            ellipsize = TextUtils.TruncateAt.END
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, UiUtils.WRAP_CONTENT, 1f)
    }

    private val notEditableSignView = LayoutDrawableView(context, DrawableGetter(R.drawable.ic_equalizer_screen_item_not_editable_sign)).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.WRAP_CONTENT, UiUtils.WRAP_CONTENT).apply {
            val marginH = dpToPxInt(8)
            leftMargin = marginH
            rightMargin = marginH
        }
    }

    private val menuButton = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_equalizer_screen_item_menu),
            onClick = { EqualizerSchemeDialogShower.show(id) }
    ).apply {
        bgColor = ColorGetter.TRANSPARENT
        val size = dpToPxInt(48)
        layoutParams = LinearLayout.LayoutParams(size, size)
    }

    private val onNameChangedListener: (String) -> Unit = { name -> titleView.text = name }

    private val onSelectedSchemeChangedListener: (Long) -> Unit = { updateSelectedState() }

    private var attachedToWindow = false

    init {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER

        addView(iconView)
        addView(titleView)
        addView(notEditableSignView)
        addView(menuButton)

        setOnClickListener {
            EqualizerManager.currentSchemeId = id
        }

        setOnLongClickListener {
            EqualizerSchemeDialogShower.show(id)
            true
        }
    }

    private fun bindScheme(oldId: Long, id: Long) {
        EqualizerManager.removeOnSchemeNameChangedListener(oldId, onNameChangedListener)
        if (attachedToWindow) {
            EqualizerManager.addOnSchemeNameChangedListener(id, onNameChangedListener)
        }

        val editable = EqualizerManager.isSchemeEditable(id)
        notEditableSignView.visibility = if (editable) View.GONE else View.VISIBLE

        val name = EqualizerManager.getSchemeName(id)
        onNameChangedListener.invoke(name)
        iconView.id = id

        updateSelectedState()
    }

    private fun updateSelectedState() {
        selectedState = id == EqualizerManager.currentSchemeId
    }

    private fun onSelectedStateChanged() {
        bgColor = if (selectedState) ColorGetter.SELECT else ColorGetter.BG
        titleView.setTextColor(if (selectedState) ColorGetter.PRIMARY else ColorGetter.FG)
        iconView.active = selectedState
        invalidate()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        attachedToWindow = true
        EqualizerManager.addOnSchemeNameChangedListener(id, onNameChangedListener)
        EqualizerManager.addOnSelectedSchemeSelectedListener(onSelectedSchemeChangedListener)

        updateSelectedState()
        onSelectedStateChanged()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        attachedToWindow = false
        EqualizerManager.removeOnSchemeNameChangedListener(id, onNameChangedListener)
        EqualizerManager.removeOnSelectedSchemeSelectedListener(onSelectedSchemeChangedListener)
    }

}