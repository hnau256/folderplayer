package ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise


data class SettingsItemChoiceProperty(
        private val getter: () -> SettingsItemChoiceViewItem,
        private val setter: (SettingsItemChoiceViewItem) -> Unit
) {

    fun get() = getter.invoke()

    fun set(value: SettingsItemChoiceViewItem) = setter.invoke(value)

}