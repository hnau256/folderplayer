package ru.hnau.folderplayer.activity.screen_pages.settings

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.view.header.Header
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.ui.UiUtils


class SettingsScreenPageView(context: Context) : LinearLayout(context) {

    private val header = Header(
            context = context,
            title = StringGetter(R.string.settings_screen_page_title)
    )

    private val list = SettingsList(context).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, 0, 1f)
    }

    init {
        orientation = VERTICAL
        addView(header)
        addView(list)
    }

}