package ru.hnau.folderplayer.activity.base

import android.os.Bundle
import android.os.Handler
import ru.hnau.folderplayer.activity.screen_pages.ScreenPage
import ru.hnau.folderplayer.activity.screen_pages.ScreenPagesContainer
import ru.hnau.folderplayer.utils.managers.FirebaseManager


abstract class PagesActivity : BaseActivity() {

    private var forceGoBack = false

    private val screenPagesContainer: ScreenPagesContainer by lazy {
        ScreenPagesContainer(
                context = this,
                initialPage = getInitialScreenPage()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Handler().post { setContentView(screenPagesContainer) }
    }

    fun show(page: ScreenPage<*>) {
        screenPagesContainer.show(page)
        FirebaseManager.sendEvent("Screen page showed", mapOf("name" to page::class.java.simpleName))
    }

    fun reloadViews() = screenPagesContainer.reloadViews()

    fun goToPreviousScreen(force: Boolean) {
        forceGoBack = force
        goBack()
        forceGoBack = false
    }

    override fun handleGoBack() = screenPagesContainer.handleGoBack()

    abstract fun getInitialScreenPage(): ScreenPage<*>

}