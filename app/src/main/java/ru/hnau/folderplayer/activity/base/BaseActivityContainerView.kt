package ru.hnau.folderplayer.activity.base

import android.view.View
import android.widget.LinearLayout
import ru.hnau.folderplayer.activity.view.bottom_animator.BottomAnimator
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.ui.ScreenManager
import ru.hnau.folderplayer.utils.ui.UiUtils


class BaseActivityContainerView(content: View) : BottomAnimator(getFgView(content)) {

    companion object {
        private fun getFgView(content: View) = LinearLayout(content.context).apply {
            orientation = LinearLayout.VERTICAL

            addView(View(content.context).apply {
                layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, ScreenManager.statusBarHeight)
                setBackgroundColor(ColorGetter.BG)
            })
            addView(content.apply {
                layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, 0, 1f)
            })
        }
    }

}