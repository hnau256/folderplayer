package ru.hnau.folderplayer.activity.view.button

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.text.TextUtils
import android.view.Gravity
import android.view.MotionEvent
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.ui.animations.ripple_animator.RippleAnimator

open class TextButton(
        context: Context,
        text: StringGetter,
        onClickListener: () -> Unit,
        size: Size = Size.MIDDLE,
        private val color: Color = Color.NORMAL
) : Label(
        context = context,
        text = StringGetter(text.get(context).toUpperCase()),
        size = size.textSize,
        color = color.fgColor,
        gravity = Gravity.CENTER,
        maxLines = 1,
        minLines = 1,
        ellipsize = TextUtils.TruncateAt.END
) {

    enum class Color(val fgColor: Int, val bgColor: Int) {
        NORMAL(ColorGetter.FG, ColorGetter.BG),
        ACCENT(ColorGetter.PRIMARY, ColorGetter.SELECT)
    }

    enum class Size(textSize: Int, paddingH: Int, paddingV: Int) {

        SMALL(textSize = 17, paddingH = 8, paddingV = 8),
        MIDDLE(textSize = 19, paddingH = 12, paddingV = 12),
        LARGE(textSize = 23, paddingH = 16, paddingV = 16);

        val textSize = SizeGetter(textSize)
        val paddingH = SizeGetter(paddingH)
        val paddingV = SizeGetter(paddingV)
    }

    private val rippleAnimator = RippleAnimator(
            context = context,
            onNeedRefresh = this::invalidate,
            maxBgPercentage = 0.3f
    )

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        val paddingH = size.paddingH.getInt(context)
        val paddingV = size.paddingV.getInt(context)
        setPadding(paddingH, paddingV, paddingH, paddingV)
        setOnClickListener { onClickListener.invoke() }
    }

    override fun draw(canvas: Canvas) {

        val fullRect = Rect(0, 0, width, height)

        paint.color = color.bgColor
        canvas.drawRect(fullRect, paint)


        paint.color = ColorGetter.BG_DARK
        paint.alpha = (rippleAnimator.bgPercentage * 255).toInt()
        canvas.drawRect(fullRect, paint)

        rippleAnimator.drawCircles { pos, r, alpha, _ ->
            paint.alpha = (255 * alpha).toInt()
            canvas.drawCircle(pos.x, pos.y, r, paint)
        }

        super.draw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        rippleAnimator.onTouchEvent(event)
        super.onTouchEvent(event)
        return true
    }

}