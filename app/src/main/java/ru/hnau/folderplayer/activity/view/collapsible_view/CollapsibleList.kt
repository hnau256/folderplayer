package ru.hnau.folderplayer.activity.view.collapsible_view

import android.content.Context
import android.view.Gravity
import ru.hnau.folderplayer.activity.view.list.BaseList
import ru.hnau.folderplayer.activity.view.list.BaseListAdapter
import ru.hnau.folderplayer.utils.ui.ScreenManager

class CollapsibleList<T> private constructor(
        private val list: BaseList<T>,
        initExpanded: Boolean,
        maxListHeight: Int?
) : CollapsibleView(
        content = list,
        initExpanded = initExpanded,
        maxContentHeight = maxListHeight
) {

    var adapter: BaseListAdapter<T>?
        set(value) {
            list.baseListAdapter = value
        }
        get() = list.baseListAdapter

    var fistVisibleItemPosition: Int
        set(value) {
            list.fistVisibleItemPosition = value
        }
        get() = list.fistVisibleItemPosition

    companion object {

        fun <T> create(
                context: Context,
                initExpanded: Boolean = false,
                maxContentHeight: Int? = (ScreenManager.sizePx.y * 0.25).toInt(),
                listGravity: Int = Gravity.CENTER) = CollapsibleList<T>(
                list = BaseList(context),
                initExpanded = initExpanded,
                maxListHeight = maxContentHeight

        )

    }


}