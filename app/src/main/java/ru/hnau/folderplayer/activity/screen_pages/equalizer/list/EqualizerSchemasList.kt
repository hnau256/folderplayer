package ru.hnau.folderplayer.activity.screen_pages.equalizer.list

import android.content.Context
import android.os.Handler
import ru.hnau.folderplayer.activity.view.list.BaseList
import ru.hnau.folderplayer.utils.findPos
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager


class EqualizerSchemasList(context: Context) : BaseList<Long>(
        context,
        LayoutType.VERTICAL
) {

    private val schemasAdapter = EqualizerSchemasListAdapter(context)

    private val onSchemeAdded = schemasAdapter::onSchemeAdded
    private val onSchemeRemoved = schemasAdapter::onSchemeRemoved

    private val onSelectedSchemeChangedListener: (Long) -> Unit = { id ->
        val selectedSchemePos = EqualizerManager.schemasIds.findPos { it == id }
        if (selectedSchemePos != null) {
            Handler().postDelayed({ this@EqualizerSchemasList.fistVisibleItemPosition = selectedSchemePos }, 100)
        }
    }

    init {
        baseListAdapter = schemasAdapter
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        EqualizerManager.addOnSelectedSchemeAddedListener(onSchemeAdded)
        EqualizerManager.addOnSelectedSchemeRemovedListener(onSchemeRemoved)
        EqualizerManager.addOnSelectedSchemeSelectedListener(onSelectedSchemeChangedListener)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        EqualizerManager.removeOnSelectedSchemeAddedListener(onSchemeAdded)
        EqualizerManager.removeOnSelectedSchemeRemovedListener(onSchemeRemoved)
        EqualizerManager.removeOnSelectedSchemeSelectedListener(onSelectedSchemeChangedListener)
    }


}