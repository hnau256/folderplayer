package ru.hnau.folderplayer.activity.screen_pages.files_explorer.files_explorer_list

import android.content.Context
import android.text.TextUtils
import android.view.Gravity
import android.widget.LinearLayout
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.view.ClickableLinearLayout
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.managers.bookmarks.BookmarksManager
import ru.hnau.folderplayer.utils.files_tree.item.*
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.audio.PlayerManager
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.DialogManager
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.actions.DialogAction
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.dpToPxInt
import ru.hnau.folderplayer.utils.ui.drawable.layout_drawable.LayoutDrawable
import ru.hnau.folderplayer.utils.ui.drawable.layout_drawable.LayoutDrawableView


class FilesTreeListItemView(
        context: Context,
        private val onItemRemovedCallback: (FilesTreeItem) -> Unit,
        private val onItemClickCallback: (FilesTreeItem) -> Unit
) : ClickableLinearLayout(context) {

    private val iconView = LayoutDrawableView(
            context = context,
            layoutType = LayoutDrawable.LayoutType.WRAP_CONTENT
    ).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.WRAP_CONTENT, UiUtils.WRAP_CONTENT).apply {
            val marginH = dpToPxInt(8)
            leftMargin = marginH
            rightMargin = marginH
        }
    }

    private val titleView = Label(
            context = context,
            gravity = Gravity.LEFT or Gravity.CENTER_VERTICAL,
            size = SizeGetter(20),
            color = ColorGetter.FG,
            maxLines = 1,
            minLines = 1,
            ellipsize = TextUtils.TruncateAt.END
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, UiUtils.WRAP_CONTENT, 1f).apply {
            bottomMargin = dpToPxInt(1)
        }
    }

    private val resumeWhereStoppedSign = FilesTreeListItemViewResumeWhereStoppedSignView(context).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.WRAP_CONTENT, UiUtils.WRAP_CONTENT)
    }

    var item: FilesTreeItem? = null
        set(value) {
            if (field != value) {
                field = value
                bindItem(value)
                updatePlayItemRelationState()
                resumeWhereStoppedSign.item = value
            }
        }

    private var playItemRelationState: PlayItemRelationChecker.Type = PlayItemRelationChecker.Type.NONE
        set(value) {
            if (field != value) {
                field = value
                onPlayItemRelationStateChanged()
            }
        }

    private val onCurrentPlayItemSelected = this::updatePlayItemRelationState

    init {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER

        addView(iconView)
        addView(titleView)
        addView(resumeWhereStoppedSign)

        val paddingV = dpToPxInt(8)
        setPadding(dpToPxInt(4), paddingV, dpToPxInt(8), paddingV)

        setOnClickListener { item?.let { onItemClickCallback.invoke(it) } }
        setOnLongClickListener { onLongClicked() }
    }

    private fun updatePlayItemRelationState(state: PlayerManager.State = PlayerManager.state) {
        playItemRelationState = state.currentItem?.let { item?.getPlayItemRelation(it) } ?: PlayItemRelationChecker.Type.NONE
        resumeWhereStoppedSign.itemIsPlaying = playItemRelationState != PlayItemRelationChecker.Type.NONE
    }

    private fun onPlayItemRelationStateChanged() {
        bgColor = playItemRelationState.bgColor
        titleView.setTextColor(playItemRelationState.fgColor)
    }

    private fun bindItem(item: FilesTreeItem?) {
        iconView.icon = item?.icon
        titleView.text = item?.getTitle()?.get(context) ?: "???"
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        PlayerManager.addOnStateChangedListener(onCurrentPlayItemSelected)
        updatePlayItemRelationState()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        PlayerManager.removeOnStateChangedListener(onCurrentPlayItemSelected)
    }

    private fun onLongClicked(): Boolean {
        val item = this.item ?: return false
        if (item is BookmarkFilesTreeItem) {
            onBookmarkLongClicked(item)
            return true
        }
        if (item is OpenableFilesTreeItem && item is FsFilesTreeItem) {
            onOpenableLongClicked(item)
            return true
        }
        return false
    }

    private fun onBookmarkLongClicked(bookmark: BookmarkFilesTreeItem) {
        DialogManager.show {
            titleForActions(bookmark.getTitle())
            content(listOf(
                    DialogAction(
                            title = StringGetter(R.string.file_explorer_screen_page_item_long_clicked_dialog_item_remove_bookmark),
                            action = {
                                BookmarksManager.removeBookmark(bookmark.id)
                                onItemRemovedCallback.invoke(bookmark)
                                FirebaseManager.sendEvent("Bookmark removed")
                            }
                    )
            ))
        }
    }

    private fun onOpenableLongClicked(item: FsFilesTreeItem) {
        DialogManager.show {
            titleForActions(item.getTitle())
            content(listOf(
                    DialogAction(
                            title = StringGetter(R.string.file_explorer_screen_page_item_long_clicked_dialog_item_add_to_bookmarks),
                            action = {
                                BookmarksManager.addBookmark(item.path)
                                FirebaseManager.sendEvent("Bookmark added")
                            }
                    )
            ))
        }
    }

}