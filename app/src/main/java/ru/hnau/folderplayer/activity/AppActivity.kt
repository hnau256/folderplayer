package ru.hnau.folderplayer.activity

import android.content.Intent
import android.os.Bundle
import ru.hnau.folderplayer.activity.base.PagesActivity
import ru.hnau.folderplayer.activity.screen_pages.files_explorer.FilesExplorerScreenPage
import ru.hnau.folderplayer.utils.managers.AppActivityManager
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManagerStarter
import ru.hnau.folderplayer.utils.permission.PermissionRequester


class AppActivity : PagesActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppActivityManager.onAppActivityCreated(this)
        PlayerSessionManagerStarter.onIntentReceived(this, intent)
    }

    override fun getInitialScreenPage() = FilesExplorerScreenPage()

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        PlayerSessionManagerStarter.onIntentReceived(this, intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        AppActivityManager.onAppActivityDestroyed()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        PermissionRequester.onRequestResult(requestCode, permissions, grantResults)
    }

}
