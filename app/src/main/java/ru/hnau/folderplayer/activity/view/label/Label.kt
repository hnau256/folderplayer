package ru.hnau.folderplayer.activity.view.label

import android.content.Context
import android.text.TextUtils
import android.util.TypedValue
import android.view.Gravity
import android.widget.TextView
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.ui.FontManager


open class Label(
        context: Context,
        text: StringGetter = StringGetter(),
        viewInfo: TextViewInfo
) : TextView(context) {

    constructor(
            context: Context,
            text: StringGetter = StringGetter(),
            size: SizeGetter = SizeGetter(16),
            color: Int = ColorGetter.FG,
            gravity: Int = Gravity.CENTER_VERTICAL or Gravity.LEFT,
            maxLines: Int? = null,
            minLines: Int? = null,
            ellipsize: TextUtils.TruncateAt = TextUtils.TruncateAt.END
    ) : this(
            context = context,
            viewInfo = TextViewInfo(
                    size = size,
                    color = color,
                    gravity = gravity,
                    maxLines = maxLines,
                    minLines = minLines,
                    ellipsize = ellipsize
            ),
            text = text
    )

    init {
        FontManager.fontView(this)
        setTextSize(TypedValue.COMPLEX_UNIT_PX, viewInfo.size.get(context))
        setTextColor(viewInfo.color)
        gravity = viewInfo.gravity
        viewInfo.maxLines?.let { this.maxLines = it }
        viewInfo.minLines?.let { this.minLines = it }
        ellipsize = viewInfo.ellipsize
        this.text = text.get(context)
    }

}