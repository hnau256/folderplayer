package ru.hnau.folderplayer.activity.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.ui.ScaleManager.value
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.UiUtils.calcRoundSidesPath
import ru.hnau.folderplayer.utils.ui.UiUtils.getColorInterTwoColors
import ru.hnau.folderplayer.utils.ui.UiUtils.setColorAlpha
import ru.hnau.folderplayer.utils.ui.animations.TwoStateAnimator
import ru.hnau.folderplayer.utils.ui.dpToPxInt

class CheckBox(context: Context, private val onStateChangedListener: ((Boolean) -> Unit)? = null, initialState: Boolean = false) : View(context) {

    private val BG_WIDTH = dpToPxInt(28)
    private val BG_HEIGHT = dpToPxInt(12)
    private val DOT_SIZE = dpToPxInt(18)
    private val DOT_CIRCLE_SIZE = dpToPxInt(32)
    private val WIDTH = BG_WIDTH + DOT_CIRCLE_SIZE
    private val HEIGHT = DOT_CIRCLE_SIZE

    private val BG_ACTIVE_COLOR = ColorGetter.PRIMARY
    private val BG_INACTIVE_COLOR = ColorGetter.FG_TRANSPARENT_50
    private val DOT_ACTIVE_COLOR = ColorGetter.FG
    private val DOT_INACTIVE_COLOR = ColorGetter.FG
    private val DOT_CIRCLE_ACTIVE_COLOR = setColorAlpha(ColorGetter.PRIMARY, 0.15f)
    private val DOT_CIRCLE_INACTIVE_COLOR = setColorAlpha(ColorGetter.FG, 0.1f)

    private val fillPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        setOnClickListener { onClicked() }
    }

    fun onClicked() {
        checked = !checked
    }

    private var updatingChecked = false

    var checked: Boolean = initialState
        set(value) {
            if (field != value) {
                field = value
                if (!updatingChecked) {
                    setChecked(value)
                }
            }
        }

    private var checkedFloat: Float = if (initialState) 1f else 0f
        set(value) {
            field = value
            invalidate()
        }

    private var pressedPercentage: Float = 0f
        set(value) {
            field = value
            invalidate()
        }

    private val checkedAnimator = TwoStateAnimator(initialState).apply {
        iterator = { checkedFloat = it }
    }

    private val pressedAnimator = TwoStateAnimator().apply {
        iterator = { pressedPercentage = it }
    }

    fun setChecked(checked: Boolean, animate: Boolean = true) {
        onStateChangedListener?.invoke(checked)
        checkedAnimator.addNewTarget(checked, !animate)

        updatingChecked = true
        this.checked = checked
        updatingChecked = false
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        val cx = paddingLeft + (width - paddingLeft - paddingRight) / 2
        val cy = paddingTop + (height - paddingTop - paddingBottom) / 2
        val dotOffset = (BG_WIDTH - BG_HEIGHT) * (checkedFloat - 0.5f)

        drawBg(canvas, cx, cy)
        drawDotCircle(canvas, cx, cy, dotOffset)
        drawDot(canvas, cx, cy, dotOffset)
    }

    private fun drawBg(canvas: Canvas, cx: Int, cy: Int) {
        fillPaint.color = getColorInterTwoColors(BG_INACTIVE_COLOR, BG_ACTIVE_COLOR, checkedFloat)
        val path = calcRoundSidesPath(cx - BG_WIDTH / 2, cy - BG_HEIGHT / 2, BG_WIDTH, BG_HEIGHT)
        canvas.drawPath(path, fillPaint)
    }

    private fun drawDotCircle(canvas: Canvas, cx: Int, cy: Int, dotOffset: Float) {
        if (pressedPercentage <= 0) {
            return
        }

        val x = cx + dotOffset
        val y = cy.toFloat()
        val r = DOT_CIRCLE_SIZE * pressedPercentage / 2f

        fillPaint.color = getColorInterTwoColors(DOT_CIRCLE_INACTIVE_COLOR, DOT_CIRCLE_ACTIVE_COLOR, checkedFloat)
        canvas.drawCircle(x, y, r, fillPaint)
    }

    private fun drawDot(canvas: Canvas, cx: Int, cy: Int, dotOffset: Float) {
        val x = cx + dotOffset
        val y = cy.toFloat()
        val r = DOT_SIZE / 2f

        fillPaint.color = getColorInterTwoColors(DOT_INACTIVE_COLOR, DOT_ACTIVE_COLOR, checkedFloat)
        canvas.drawCircle(x, y, r, fillPaint)
    }

    override fun drawableStateChanged() {
        super.drawableStateChanged()
        pressedAnimator.addNewTarget(isPressed)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = WIDTH + paddingLeft + paddingRight
        val height = HEIGHT + paddingTop + paddingBottom
        setMeasuredDimension(
                UiUtils.getDefaultSize(width, widthMeasureSpec),
                UiUtils.getDefaultSize(height, heightMeasureSpec)
        )
    }

}