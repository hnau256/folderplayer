package ru.hnau.folderplayer.activity.view.views_switcher.layout_manager

import android.graphics.Rect
import android.view.View

/**
 * Размер и положение прокручиваемого элемента внутри ViewsSwitcher
 */
interface ViewsSwitcherLayoutManager {

    fun layoutView(view: View, width: Int, height: Int): Rect

}