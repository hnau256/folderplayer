package ru.hnau.folderplayer.activity.screen_pages.settings.item.view

import android.content.Context
import android.view.Gravity
import android.widget.LinearLayout
import ru.hnau.folderplayer.activity.screen_pages.settings.item.SettingsItem
import ru.hnau.folderplayer.activity.screen_pages.settings.item.SettingsProperty
import ru.hnau.folderplayer.activity.view.CheckBox
import ru.hnau.folderplayer.activity.view.ClickableLinearLayout
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class SettingsItemBooleanView(context: Context) : ClickableLinearLayout(context) {

    private val titleView = SettingsItemTitleLabelView(context).apply {
        layoutParams = LinearLayout.LayoutParams(0, UiUtils.WRAP_CONTENT, 1f)
    }

    private val checkBox = CheckBox(
            context,
            { property?.set(it) }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.WRAP_CONTENT, UiUtils.WRAP_CONTENT)
    }

    var title: StringGetter? = null
        set(value) {
            if (field != value && value != null) {
                field = value
                titleView.text = value.get(context)
            }
        }

    var property: SettingsProperty<Boolean>? = null
        set(value) {
            if (field != value && value != null) {
                field = value
                checkBox.setChecked(value.get(), false)
            }
        }

    init {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER
        setBackgroundColor(ColorGetter.BG)

        val paddingH = SettingsItem.PADDING_H
        val paddingV = SettingsItem.PADDING_V - dpToPxInt(8)
        setPadding(paddingH, paddingV, paddingH - dpToPxInt(14), paddingV)

        addView(titleView)
        addView(checkBox)

        setOnClickListener { checkBox.onClicked() }
    }


}