package ru.hnau.folderplayer.activity.screen_pages.files_explorer.player_control_panel

import android.content.Context
import android.text.TextUtils
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.activity.view.label.TextViewInfo
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter


class PlayerControlPanelTimeView(
        context: Context,
        gravity: Int
) : Label(
        context = context,
        viewInfo = TextViewInfo(
                ellipsize = TextUtils.TruncateAt.END,
                minLines = 1,
                maxLines = 1,
                gravity = gravity,
                color = ColorGetter.FG,
                size = SizeGetter(16)
        )
) {

    var time = 0
        set(value) {
            if (field != value) {
                field = value
                onTimeChanged()
            }
        }

    var showHours = false
        set(value) {
            if (field != value) {
                field = value
                onTimeChanged()
            }
        }

    init {
        onTimeChanged()
    }

    private fun onTimeChanged() {
        text = timeToStr(time, showHours)
    }

    private fun timeToStr(time: Int, showHours: Boolean) = Utils.timeToStr(time / 1000, showHours)


}